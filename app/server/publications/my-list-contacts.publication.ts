import {Constants} from "../../both/Constants";
import {MyListContactsCollection} from "../../both/collections/my-list-contacts";

Meteor.publish(Constants.PUBLICATIONS.MY_LIST_CONTACTS, function () {
    return MyListContactsCollection.find({userId: this.userId});
});