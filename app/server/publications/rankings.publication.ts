import {Constants} from "../../both/Constants";
import {EventContactsCollection} from "../../both/collections/event-contacts.collection";
declare var ReactiveAggregate;

Meteor.publish(Constants.PUBLICATIONS.RANKINGS, function (programIds:Array<string>) {
    let pipeline:any = [{
        $match: {
            programId: {
                $in: programIds
            }
        }
    }, {
        $sort: {
            updated: 1
        }
    }, {
        $group: {
            _id: {
                userId: "$userId",
                programId: "$programId"
            },
            callPoints: {
                $sum: "$eventPoints.call"
            },
            textPoints: {
                $sum: "$eventPoints.text"
            },
            paidPoints: {
                $sum: "$eventPoints.paid"
            },
            commitmentPoints: {
                $sum: "$eventPoints.commitment"
            },
            lastUpdated: {
                $last: "$updated"
            }
        }
    }, {
        $project: {
            _id: {$concat: ["$_id.userId", "$_id.programId"]},
            userId: "$_id.userId",
            programId: "$_id.programId",
            totalPoints: {$add: ["$callPoints", "$textPoints", "$paidPoints", "$commitmentPoints"]},
            lastUpdated: '$lastUpdated'
        }
    }];

    ReactiveAggregate(this, EventContactsCollection, pipeline, {
        // and send the results to another collection called below
        clientCollection: "rankings"
    });
});

Meteor.publish(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, function (programIds:Array<string>) {
    if (programIds && programIds.length > 0) {
        let query:any = {
            $or: []
        };
        programIds.forEach((programId:string) => {
            let programQuery:any = {};
            let queryKey:string = "roles." + programId;
            programQuery[queryKey] = {
                $in: [Constants.ROLES.MEMBER]
            };
            query["$or"].push(programQuery);
        });

        return Meteor.users.find(query, {
            fields: {
                profile: 1,
                emails: 1,
                roles: 1
            }
        });
    }
});
