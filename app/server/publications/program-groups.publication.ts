import {Constants} from "../../both/Constants";
import {ProgramGroupsCollection} from "../../both/collections/program-groups.collection";
Meteor.publish(Constants.PUBLICATIONS.PROGRAM_GROUPS, function (programIds:Array<string>) {
    if (Array.isArray(programIds) && programIds.length > 0) {
        return ProgramGroupsCollection.find({programId: {$in: programIds}});
    }
});