import {Constants} from "../../../both/Constants";
import {ProgramsCollection} from "../../../both/collections/programs.collection";
import {IProgramInfo} from "../../../both/models/program-info.model";
import * as moment from "moment";
import {IOnlineParticipantRequest} from "../../models/participant-registration.model";
import {SmsRequestObject} from "../../models/sms-request.model";
import {IPushNotification} from "../../../both/models/push-notification.model";
import {NotificationHistoryCollection} from "../../collections/notification-history.collection";
var Future = Npm.require('fibers/future');
var bcrypt = Npm.require('bcrypt');

declare var console;
declare var Accounts;
declare var Roles;
declare var SHA256;
declare var Push;

export class MeteorMethods {
    private readonly bcryptSaltRounds = 13;

    public init():void {
        Meteor.methods({
            'updateAccountInfo': (updatedUser:Meteor.User) => {
                var user:Meteor.User = this.checkForUser();
                let userEmails:Array<any> = user.emails;
                if (updatedUser.emails) {
                    if (!user.emails || (user.emails && !user.emails[0]) || user.emails[0].address !== updatedUser.emails[0].address) {
                        console.log("Update email address");
                        if (updatedUser.emails[0].address) {
                            console.log("Check for existing email address: ", updatedUser.emails[0].address);
                            let existingUser:Meteor.User = Accounts.findUserByEmail(updatedUser.emails[0].address);
                            if (existingUser) {
                                throw new Meteor.Error(Constants.METEOR_ERRORS.ALREADY_EXISTS,
                                    Constants.METEOR_ERRORS.EMAIL_EXISTS);
                            }
                            userEmails = [{address: updatedUser.emails[0].address, verified: false}];
                        } else {
                            userEmails = [];
                        }
                    }
                }

                if (user.username !== updatedUser.username) {
                    console.log("Update username");
                    let existingUser:Meteor.User = Accounts.findUserByUsername(updatedUser.username);
                    if (existingUser) {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.ALREADY_EXISTS,
                            Constants.METEOR_ERRORS.USERNAME_EXISTS);
                    }
                }

                var future = new Future();
                Meteor.users.update(user._id, {
                    $set: {
                        "profile.name.given": updatedUser.profile.name.given,
                        "profile.name.family": updatedUser.profile.name.family,
                        "profile.name.display": updatedUser.profile.name.given + " " + updatedUser.profile.name.family,
                        "profile.picture": updatedUser.profile.picture,
                        "emails": userEmails,
                        "username": updatedUser.username
                    }
                }, {
                    multi: false,
                    upsert: false
                }, (error, result) => {
                    if (error) {
                        console.error("Error updating account info: ", error);
                        future.throw(error);
                    } else {
                        console.log("Successfully updated account info.");
                        future.return(result);
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            'sendPasswordResetSMS': (data:{username:string}) => {
                console.log("send password reset SMS for username: ", data.username);
                var user:Meteor.User = Accounts.findUserByUsername(data.username);
                if (!user) {
                    console.log("User not found");
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND,
                        Constants.METEOR_ERRORS.USER_NOT_FOUND);
                }

                var token = Random.secret();
                var when = new Date();
                var tokenRecord = {
                    token: token,
                    phone: data.username,
                    when: when,
                    reason: 'reset'
                };
                Meteor.users.update(user._id, {
                    $set: {
                        "services.password.reset": tokenRecord
                    }
                });
                // before passing to template, update user object with new token
                (Meteor as any)._ensure(user, 'services', 'password').reset = tokenRecord;
                console.log("token: " + token);

                var href:string = Meteor.settings.public["host"]
                    + Constants.ROUTES.RESET_PASSWORD + "?token=" + token;

                // Shorten link
                var future = new Future();
                Meteor.call("shortenUrl", href, (error, result) => {
                    if (error) {
                        console.error("Shorten Link Error: ", error);
                    } else {
                        if (result.data && result.data.shortLink) {
                            href = result.data.shortLink
                        }
                    }

                    var smsMessage:string = "Password Reset Link: " + href + " for " + Meteor.settings.public["appName"];

                    let smsEndpoint:string = Meteor.settings.private["gaoRest"]["baseUrl"];
                    if (smsEndpoint) {
                        smsEndpoint += Meteor.settings.private["gaoRest"]["endpoints"]["sms"];
                    }
                    let clientId:string = Meteor.settings.private["gaoRest"]["credentials"]["clientId"];
                    let clientSecret:string = Meteor.settings.private["gaoRest"]["credentials"]["clientSecret"];
                    let headers:any = {
                        "Client-Id": clientId,
                        "Client-Secret": clientSecret
                    };

                    let postData:SmsRequestObject = new SmsRequestObject({
                        recipientPhoneNumber: user.username,
                        message: smsMessage,
                        isOptInMessage: true,
                    });

                    console.log("postData: ", postData);
                    HTTP.call("POST", smsEndpoint, {
                        headers: headers,
                        data: postData,
                        timeout: 20 * 1000
                    }, function (error, response:any) {
                        if (error) {
                            future.throw(error)
                        } else {
                            console.log("response: ", response);

                            if (!response.data || !response.data.sendSmsResponse) {
                                future.return({
                                    success: false,
                                    message: "Invalid response scheme"
                                });
                            } else if (!response.data.sendSmsResponse.smsIdentifier) {
                                future.return({
                                    success: false,
                                    message: "Missing SMS Identifier"
                                });
                            } else {
                                future.return({
                                    success: true,
                                    message: "Successfully sent password reset link."
                                });
                            }
                        }
                    });
                });

                try {
                    return future.wait();
                } catch (error) {
                    console.error("Send SMS Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            'resetPasswordFromSMS': (data:{token:string, password:any}) => {
                var self = this;

                check(data.token, String);
                check(data.password, {digest: String, algorithm: String});

                var user = Meteor.users.findOne({
                    "services.password.reset.token": data.token
                });
                if (!user) {
                    throw new Meteor.Error(403, "Token expired");
                }
                var when = user.services.password.reset.when;
                var reason = user.services.password.reset.reason;
                var tokenLifetimeMs = Accounts._getPasswordResetTokenLifetimeMs();

                var currentTimeMs = Date.now();
                if ((currentTimeMs - when) > tokenLifetimeMs)
                    throw new Meteor.Error(403, "Token expired");
                var phone = user.services.password.reset.phone;
                if (phone !== user.username) {
                    return {
                        userId: user._id,
                        error: new Meteor.Error(403, "Token has invalid phone number")
                    };
                }

                var hashed = MeteorMethods.hashPassword(data.password);

                var future = new Future();
                Meteor.users.update({
                    _id: user._id,
                    username: phone,
                    'services.password.reset.token': data.token
                }, {
                    $set: {
                        'services.password.bcrypt': hashed,
                    },
                    $unset: {
                        'services.password.reset': 1,
                        'services.password.srp': 1
                    }
                }, {
                    multi: false,
                    upsert: false
                }, (error, affectedRecords) => {
                    if (error) {
                        console.error("Error resetting password: " + error);
                        future.throw(error);
                    } else {
                        if (affectedRecords !== 1) {
                            future.return({
                                userId: user._id,
                                error: new Meteor.Error(403, "Invalid phone number")
                            });
                        } else {
                            // Replace all valid login tokens with new ones (changing
                            // password should invalidate existing sessions).
                            Accounts._clearAllLoginTokens(user._id);

                            var stampedLoginToken = Accounts._generateStampedLoginToken();
                            Accounts._insertLoginToken(user._id, stampedLoginToken);
                            var loginToken:string = stampedLoginToken.token;
                            future.return({
                                success: true,
                                userId: user._id,
                                loginToken: loginToken
                            });
                        }
                    }
                });

                try {
                    return future.wait();
                } catch (error) {
                    throw error;
                }
            },
            '/auth/fingerprint/android/credentials/secret': (data:{
                deviceId:string
            }) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();
                    var secret = bcrypt.hashSync(user._id + data.deviceId, self.bcryptSaltRounds);
                    var hash = bcrypt.hashSync(secret, self.bcryptSaltRounds);
                    // Store hash in your password DB.
                    Meteor.users.update(user._id, {
                        $set: {
                            "services.fingerprint.bcrypt": hash
                        }
                    }, {
                        multi: false,
                        upsert: false
                    }, (error, result) => {
                        if (error) {
                            console.log("Error saving fingerprint services: ", error);
                            future.throw(error);
                        } else {
                            future.return({secret: secret});
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            '/auth/fingerprint/android/credentials/secret/verify': (data:{
                username:string,
                secret:string
            }) => {
                var self = this;
                var user:Meteor.User = Accounts.findUserByUsername(data.username);
                if (!user) {
                    throw new Meteor.Error("account-not-found", "Phone number not found.");
                } else {
                    var hash = null;
                    var verified = false;
                    if (user.services.fingerprint && user.services.fingerprint.bcrypt) {
                        hash = user.services.fingerprint.bcrypt;
                    }
                    if (hash) {
                        verified = bcrypt.compareSync(data.secret, hash);
                    }

                    var loginToken:string;
                    if (verified) {
                        var stampedLoginToken = Accounts._generateStampedLoginToken();
                        Accounts._insertLoginToken(user._id, stampedLoginToken);
                        loginToken = stampedLoginToken.token;
                    }

                    return {isVerified: verified, loginToken: loginToken};
                }
            },
            '/auth/fingerprint/android/credentials/token/save': (data:{
                deviceId:string,
                token:string
            }) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();
                    Meteor.users.update(user._id, {
                        $set: {
                            "services.fingerprint.token": data.token,
                            "profile.deviceId": data.deviceId
                        }
                    }, {
                        multi: false,
                        upsert: false
                    }, (error, result) => {
                        if (error) {
                            console.log("Error saving fingerprint token: ", error);
                            future.throw(error);
                        } else {
                            future.return(true);
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            '/auth/fingerprint/android/credentials/token': (data:{
                username:string,
                deviceId:string
            }) => {
                var self = this;
                var user:Meteor.User = Accounts.findUserByUsername(data.username);
                if (!user) {
                    throw new Meteor.Error("account-not-found", "Phone number not found.");
                } else {
                    if (user.profile.deviceId === data.deviceId &&
                        user.services.fingerprint &&
                        user.services.fingerprint.token) {
                        return {token: user.services.fingerprint.token};
                    } else {
                        throw new Meteor.Error(Constants.METEOR_ERRORS.FINGERPRINT_NOT_ENABLED,
                            "Fingerprint authentication is not enabled for this user.");
                    }
                }
            },
            '/auth/fingerprint/disable': () => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                if (user) {
                    var future = new Future();

                    Meteor.users.update(user._id, {
                        $set: {
                            "profile.deviceId": null
                        }
                    }, {
                        multi: false,
                        upsert: false
                    }, (error, result) => {
                        if (error) {
                            console.log("Error disabling fingerprintAuthentication: ", error);
                            future.throw(error);
                        } else {
                            future.return(true);
                        }
                    });

                    try {
                        return future.wait();
                    } catch (error) {
                        throw error;
                    }
                }
            },
            'programRegistration': (data:{registrationCode:number}) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                var program:IProgramInfo = ProgramsCollection.findOne({
                    "contract.contractNumber": Number(data.registrationCode)
                });
                if (!program) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.INVALID_REGISTRATION_CODE, "Invalid registration code.");
                }
                return program;
            },
            'confirmProgramRegistration': (data:{programId:string}) => {
                var self = this;
                var user:Meteor.User = self.checkForUser();  // throws errors
                var program:IProgramInfo = ProgramsCollection.findOne({
                    _id: data.programId
                });

                if (!program) {
                    throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Program ID");
                }

                var future = new Future();
                var onlineId = "24CEMFF";
                if (program.contract.contractNumber === 987654321) {
                    MeteorMethods.registerParticipantForProgram(user, program, onlineId, future);
                } else {

                    let participantRequestEndpoint:string = Meteor.settings.private["gaoRest"]["baseUrl"];
                    if (participantRequestEndpoint) {
                        participantRequestEndpoint += Meteor.settings.private["gaoRest"]["endpoints"]["participantRegistration"];
                    }
                    let clientId:string = Meteor.settings.private["gaoRest"]["credentials"]["clientId"];
                    let clientSecret:string = Meteor.settings.private["gaoRest"]["credentials"]["clientSecret"];
                    let headers:any = {
                        "Client-Id": clientId,
                        "Client-Secret": clientSecret
                    };

                    let postData:IOnlineParticipantRequest = {
                        onlineParticipantRequest: {
                            isSupportOurSale: !program.contract.contractId,
                            contractNumber: program.contract.contractNumber,
                            contractId: program.contract.contractId,
                            phoneNumber: Number(user.username),
                            firstName: user.profile.name.given,
                            lastName: user.profile.name.family
                        }
                    };

                    if (!participantRequestEndpoint) {
                        MeteorMethods.registerParticipantForProgram(user, program, onlineId, future);
                    } else {
                        console.log("postData: ", postData);
                        HTTP.call("POST", participantRequestEndpoint, {
                            headers: headers,
                            data: postData,
                            timeout: 20 * 1000
                        }, function (error, response:any) {
                            if (error) {
                                future.throw(error)
                            } else {
                                console.log("response: ", response);

                                if (!response.data || !response.data.onlineParticipantResponse) {
                                    future.return({
                                        success: false,
                                        message: "Invalid response scheme"
                                    });
                                } else if (!response.data.onlineParticipantResponse.onlineStudentIdentifier) {
                                    future.return({
                                        success: false,
                                        message: "Response missing online ID."
                                    });
                                } else {
                                    onlineId = response.data.onlineParticipantResponse.onlineStudentIdentifier;
                                    MeteorMethods.registerParticipantForProgram(user, program, onlineId, future);
                                }
                            }
                        });
                    }
                }
                try {
                    return future.wait();
                } catch (error) {
                    console.error("Participant Registration Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            },
            // 'saveContactInfo': (contact:IMyListContact) => {
            //     var self = this;
            //     var user:Meteor.User = self.checkForUser();  // throws errors
            //     var future = new Future();
            //     let contactId:string = contact._id;
            //     delete contact._id;
            //     let hiddenWithNoEventContacts:boolean = false;
            //     if (contactId && contact.hidden) {
            //         let communications:number = EventContactsCollection.find({contactId: contactId}).count();
            //         hiddenWithNoEventContacts = (communications === 0);
            //
            //     }
            //     if (hiddenWithNoEventContacts) {
            //         MyListContactsCollection.remove({_id: contactId}, (error, result) => {
            //             if (error) {
            //                 future.throw(error);
            //             } else {
            //                 future.return(result);
            //             }
            //         });
            //     } else {
            //         MyListContactsCollection.upsert({_id: contactId}, contact, (error, result) => {
            //             if (error) {
            //                 future.throw(error);
            //             } else {
            //                 future.return(result);
            //             }
            //         });
            //     }
            //
            //     try {
            //         return future.wait();
            //     } catch (error) {
            //         throw error;
            //     }
            // },
            // 'logEventContact': (eventContact:IEventContact) => {
            //     var self = this;
            //     var user:Meteor.User = self.checkForUser();  // throws errors
            //
            //     var program:IProgramInfo = ProgramsCollection.findOne({_id: eventContact.programId});
            //     if (!program) {
            //         throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid Program ID");
            //     }
            //
            //     eventContact.updated = moment().toISOString();
            //     delete eventContact.contact;
            //
            //     var future = new Future();
            //
            //     EventContactsCollection.upsert({_id: eventContact._id}, eventContact, (error, result) => {
            //         if (error) {
            //             future.throw(error);
            //         } else {
            //             future.return(result);
            //
            //             let eventContacts:Array<IEventContact> = EventContactsCollection.find({
            //                 userId: user._id,
            //                 programId: program._id,
            //                 hidden: {$in: [null, false]}
            //             }).fetch();
            //             if (eventContacts.length >= program.contactListGoal) {
            //                 console.log("Award points for contact goal");
            //                 if (!user.profile.programs || !user.profile.programs[program._id] || !user.profile.programs[program._id].eventPoints || !user.profile.programs[program._id].eventPoints.contactGoal) {
            //                     let modifier:any = {$set: {}};
            //                     let modifierKeyString:string = "profile.programs." + program._id + ".eventPoints.contactGoal";
            //                     modifier.$set[modifierKeyString] = program.eventPoints.contactGoal;
            //                     modifierKeyString = "profile.programs." + program._id + ".eventPoints.updated";
            //                     modifier.$set[modifierKeyString] = moment().toISOString();
            //                     Meteor.users.update({_id: user._id}, modifier, (error, result) => {
            //                         if (error) {
            //                             console.error("Error awarding points for reaching the event contact goal: ", error);
            //                         } else {
            //                             console.log("Successfully awarded points for reaching the event contact goal.");
            //                         }
            //                     });
            //                 }
            //             }
            //
            //             // Sales Goal Award
            //             var paidOrderCount:number = 0;
            //             if (eventContacts && eventContacts.length > 0) {
            //                 eventContacts.forEach((contact:IEventContact) => {
            //                     if (contact.isPaid && contact.purchased) {
            //                         paidOrderCount += Number(contact.purchased);
            //                     }
            //                 });
            //             }
            //             if (paidOrderCount >= program.salesGoal) {
            //                 if (!user.profile.programs || !user.profile.programs[program._id] || !user.profile.programs[program._id].eventPoints || !user.profile.programs[program._id].eventPoints.salesGoalAward) {
            //                     let modifier:any = {$set: {}};
            //                     let modifierKeyString:string = "profile.programs." + program._id + ".eventPoints.salesGoalAward";
            //                     modifier.$set[modifierKeyString] = program.eventPoints.salesGoalAward;
            //                     modifierKeyString = "profile.programs." + program._id + ".eventPoints.updated";
            //                     modifier.$set[modifierKeyString] = moment().toISOString();
            //                     Meteor.users.update({_id: user._id}, modifier, (error, result) => {
            //                         if (error) {
            //                             console.error("Error awarding points for reaching the sales goal: ", error);
            //                         } else {
            //                             console.log("Successfully awarded points for reaching the sales goal.");
            //                         }
            //                     });
            //                 }
            //             }
            //         }
            //     });
            //
            //     try {
            //         return future.wait();
            //     } catch (error) {
            //         throw error;
            //     }
            // }
            "receivePushNotification": (notification:IPushNotification) => {
                let notificationId:string = notification.payload._id;
                NotificationHistoryCollection.update({
                    _id: notificationId
                }, {
                    $push: {
                        receivedAt: {
                            userId: Meteor.userId(),
                            time: moment().toISOString()
                        }
                    }
                });
            },
            "shortenUrl": (url:string) => {
                let apiKey = Meteor.settings.private["firebaseDynamicLinkApi"]["apiKey"];
                let endpoint = Meteor.settings.private["firebaseDynamicLinkApi"]["endpoint"];
                let dynamicLinkEndpoint = endpoint + apiKey;
                let postData:any = {
                    dynamicLinkInfo: {
                        dynamicLinkDomain: Meteor.settings.private["firebaseDynamicLinkApi"]["domainUriPrefix"],
                        link: url
                    },
                    suffix: {
                        option: "SHORT"
                    }
                };
                let future = new Future();
                HTTP.call("POST", dynamicLinkEndpoint, {
                    headers: {
                        "Content-Type": "application/json"
                    },
                    data: postData,
                    timeout: 20 * 1000
                }, function (error, response:any) {
                    if (error) {
                        future.throw(error)
                    } else {
                        future.return(response);
                    }
                });
                try {
                    return future.wait();
                } catch (error) {
                    console.error("Google URL Shortener API Error: ", error);
                    if (error.code === Constants.METEOR_ERRORS.TIMEDOUT) {
                        error = new Meteor.Error(Constants.METEOR_ERRORS.TIMEDOUT);
                    }
                    throw error;
                }
            }
        });
    }

    private checkForUser():Meteor.User {
        var currentUserId = Meteor.userId();
        var user:Meteor.User;
        if (!currentUserId) {
            throw new Meteor.Error(Constants.METEOR_ERRORS.SIGN_IN, "Please sign in.");
        } else {
            user = Meteor.users.findOne(currentUserId);
            if (!user) {
                throw new Meteor.Error(Constants.METEOR_ERRORS.ACCOUNT_NOT_FOUND, "Invalid User ID");
            }
        }
        return user;
    }

    // Given a 'password' from the client, extract the string that we should
    // bcrypt. 'password' can be one of:
    //  - String (the plaintext password)
    //  - Object with 'digest' and 'algorithm' keys. 'algorithm' must be "sha-256".
    public static getPasswordString(password) {
        if (typeof password === "string") {
            password = SHA256(password);
        } else { // 'password' is an object
            if (password.algorithm !== "sha-256") {
                throw new Error("Invalid password hash algorithm. " +
                    "Only 'sha-256' is allowed.");
            }
            password = password.digest;
        }
        return password;
    };

    // Use bcrypt to hash the password for storage in the database.
    // `password` can be a string (in which case it will be run through
    // SHA256 before bcrypt) or an object with properties `digest` and
    // `algorithm` (in which case we bcrypt `password.digest`).
    public static hashPassword(password) {
        password = this.getPasswordString(password);
        return bcrypt.hashSync(password, Accounts._bcryptRounds);
    };

    public static registerParticipantForProgram(user:Meteor.User, program:IProgramInfo, onlineId:string, future):void {
        let modifier:any = {$set: {}};
        let modifierKeyString:string = "profile.onlineIds." + program.contract.contractNumber;
        modifier.$set[modifierKeyString] = onlineId;
        modifierKeyString = "profile.programs." + program._id + ".eventPoints.updated";
        modifier.$set[modifierKeyString] = moment().toISOString();
        Meteor.users.update({
                _id: user._id
            },
            modifier, {
                multi: false,
                upsert: false
            }, (error, result) => {
                if (error) {
                    console.error("Error saving participant online ID: ", error);
                    future.throw(error);
                } else {
                    console.log("Successfully saved participant online ID.");
                    if (!Roles.userIsInRole(user._id, Constants.ROLES.MEMBER, program._id)) {
                        Roles.addUsersToRoles(user._id, [Constants.ROLES.MEMBER], program._id);
                    }

                    if (!Roles.userIsInRole(user._id, Constants.ROLES.MEMBER, program._id)) {
                        Roles.addUsersToRoles(user._id, [Constants.ROLES.MEMBER], program._id);
                    }

                    future.return({
                        success: true,
                        message: "Successfully registered user as member of " + program.name,
                        programName: program.name,
                        programId: program._id,
                        onlineId: onlineId
                    });

                    MeteorMethods.pushRegistrationAwardNotification(user, program);
                }
            });
    }

    public static pushRegistrationAwardNotification(user:Meteor.User, program:IProgramInfo):void {
        var notification:IPushNotification = {};
        notification.sentAt = moment().toISOString();
        notification.from = user._id;
        notification.action = Constants.PUSH_NOTIFICATION_ACTIONS.AWARD;
        notification.title = "Award Earned";
        notification.message = "Congratulations!  You earned the registration award and received "
            + program.eventPoints.registration + " points!";

        var payload:any = {
            action: notification.action,
            userId: user._id,
            programId: program._id,
            title: notification.title,
            message: notification.message,
            awardKey: Constants.AWARD_KEYS.REGISTRATION
        };
        notification.payload = payload;

        NotificationHistoryCollection.insert(notification, function (error, notificationId) {
            if (error) {
                console.error("Error inserting notification history: ", error);
            } else {
                notification._id = notificationId;
                payload._id = notificationId;
                console.log("Sending award notification: ", notification);
                Push.send({
                    from: user.profile.name.display,
                    title: notification.title,
                    text: notification.message,
                    sound: 'default',
                    payload: payload,
                    query: {
                        userId: {
                            $in: [user._id]
                        }
                    },
                    notId: NotificationHistoryCollection.find().count()
                });
            }
        });
    }
}