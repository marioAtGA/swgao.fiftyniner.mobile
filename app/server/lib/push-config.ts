import {Constants} from "../../both/Constants";
declare var Push;

var environment:string = Meteor.settings.public["environment"];
var isProduction:boolean = (environment === Constants.ENVIRONMENT.PRODUCTION || environment === Constants.ENVIRONMENT.TEST);

Push.Configure({
    apn: {
        certData: Assets.getText('ios/BlitzPushProdCert.pem'),
        keyData: Assets.getText('ios/BlitzPushProdKey.pem'),
        passphrase: 'F1ftyN1n3rP4shP@ssw0rd',
    },
    "apn-dev": {
        certData: Assets.getText('ios/BlitzPushDevCert.pem'),
        keyData: Assets.getText('ios/BlitzPushDevKey.pem'),
        passphrase: 'F1ftyN1n3rP4shP@ssw0rd',
    },
    gcm: {
        apiKey: 'AAAAfe67R0g:APA91bF3i2ydoloOw5ImGGKHiQPXn8rm5shTbKxwvvpBDTufSyMlB-jNWmG5oHrPtVTXuONWe7FpCjt_nNtsl8V882wjSPvHwlLleX2Qiai8tLN8ktH2XISHT7ZmnRAb1ROPt51Tugjv',
        projectNumber: 540876162888
    },
    production: isProduction,
    sound: true,
    badge: true,
    alert: true,
    vibrate: true,
    //sendInterval: 15000, //Configurable interval between sending
    sendBatchSize: 1, //Configurable number of notifications to send per batch
    keepNotifications: false,
});

