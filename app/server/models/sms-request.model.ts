import {Constants} from "../../both/Constants";
export interface ISmsRequest {
        recipientPhoneNumber:string,
        message:string,
        isOptInMessage:boolean,
        priority?:string,
        country?:string

}

export class SmsRequestObject  {
    public sendSmsRequest:ISmsRequest;

    constructor(smsRequest:ISmsRequest) {
        this.sendSmsRequest = {
            recipientPhoneNumber: Constants.EMPTY_STRING,
            message: Constants.EMPTY_STRING,
            isOptInMessage: false,
            priority: "Realtime",
            country: "US"
        };
        if (smsRequest.hasOwnProperty("recipientPhoneNumber")) {
            this.sendSmsRequest.recipientPhoneNumber = smsRequest.recipientPhoneNumber;
        }
        if (smsRequest.hasOwnProperty("message")) {
            this.sendSmsRequest.message = smsRequest.message;
        }
        if (smsRequest.hasOwnProperty("isOptInMessage")) {
            this.sendSmsRequest.isOptInMessage = smsRequest.isOptInMessage;
        }
        if (smsRequest.hasOwnProperty("priority")) {
            this.sendSmsRequest.priority = smsRequest.priority;
        }
        if (smsRequest.hasOwnProperty("country")) {
            this.sendSmsRequest.country = smsRequest.country;
        }
    }
}

export interface ISendSmsResponseObject {
    sendSmsResponse: {
        smsIdentifier:number
    }
}