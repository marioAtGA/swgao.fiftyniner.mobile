export interface IOnlineParticipantRequest {
    onlineParticipantRequest:{
        isSupportOurSale:boolean,
        contractNumber:number,
        contractId:number,
        phoneNumber:number
        firstName:string
        lastName:string
    }
}

export interface IOnlineParticipantResponse {
    onlineParticipantResponse:{
        onlineStudentIdentifier:string
    }
}