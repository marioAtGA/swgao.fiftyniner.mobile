import {Constants} from "./Constants";

export class Utils {
    public static removeNonDigits(phoneNumber:string):string {
        var regex = /^\d*$/;
        var value:string = phoneNumber;
        if (value && !regex.test(value)) {
            var result = value.replace(/[^0-9]/g, Constants.EMPTY_STRING);
            phoneNumber = result;
        }
        return phoneNumber;
    }
}