import {IProgramInfo} from "../models/program-info.model";
declare var Ground;

export const ProgramsCollection = new Mongo.Collection<IProgramInfo>("programs");

if (Meteor.isCordova) {
    Ground.Collection(ProgramsCollection);
}

if (Meteor.isServer) {
    ProgramsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    ProgramsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}
