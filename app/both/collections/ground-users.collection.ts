declare var Ground;

if (Meteor.isCordova) {
    Ground.Collection(Meteor.users);
    var roles = (Mongo.Collection as any).get('roles');
    if (roles) {
        Ground.Collection(roles);
    }
}