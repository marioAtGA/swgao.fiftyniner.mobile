import {IRankings} from "../models/rankings.model";
declare var Ground;

export const RankingsCollection = new Mongo.Collection<IRankings>("rankings");

if (Meteor.isCordova) {
    Ground.Collection(RankingsCollection);
}

if (Meteor.isServer) {
    RankingsCollection.allow({
        insert: function (userId, doc) {
            return false;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return false;
        },

        remove: function (userId, doc) {
            return false;
        }
    });

    RankingsCollection.deny({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });
}
