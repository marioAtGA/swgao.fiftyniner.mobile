import {IMyListContact} from "../models/my-list-contact.model";
declare var Ground;

export const MyListContactsCollection = new Mongo.Collection<IMyListContact>("my_contacts");
export const GroundMyListContactsCollection = new Ground.Collection(MyListContactsCollection, "my_contacts");

if (Meteor.isServer) {
    MyListContactsCollection.allow({
        insert: function (userId, doc) {
            return true;
        },

        update: function (userId, doc, fieldNames, modifier) {
            return true;
        },

        remove: function (userId, doc) {
            return true;
        }
    });

    // MyListContactsCollection.deny({
    //     insert: function (userId, doc) {
    //         return true;
    //     },
    //
    //     update: function (userId, doc, fieldNames, modifier) {
    //         return true;
    //     },
    //
    //     remove: function (userId, doc) {
    //         return true;
    //     }
    // });
}
