export interface IAwardInfo {
    key:string,
    imageUri:string,
    color:string,
    current?:number,
    goal?:number,
    progress?:number,
    progressBar?:any,
    reward?:number
}

export class AwardInfo implements IAwardInfo {
    public key:string;
    public imageUri:string;
    public color:string;
    public current:number = 0;
    public goal:number = 0;
    public progress:number = 0;
    public progressBar:any;
    public reward:number = 0;
    
    constructor(data?:IAwardInfo) {
        if (data) {
            this.key = data.key;
            this.imageUri = data.imageUri;
            this.color = data.color;
            if (data.hasOwnProperty("current")) {
                this.current = data.current;
            }
            if (data.hasOwnProperty("goal")) {
                this.goal = data.goal;
            }
            if (data.hasOwnProperty("progress")) {
                this.progress = data.progress;
            }
            if (data.hasOwnProperty("progressBar")) {
                this.progressBar = data.progressBar;
            }
            if (data.hasOwnProperty("reward")) {
                this.reward = data.reward;
            }
        }
    }
}