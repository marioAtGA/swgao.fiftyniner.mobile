export interface IParticipantTotals {
    _id?:string,
    points?:number,
    pointsLastUpdated?:string,
    contacts?:number,
    commitments?:number,
    commitmentSales?:number,
    purchases?:number,
    purchasedSales?:number,
    commitmentProfit?:number,
    purchasedProfit?:number
}

export class ParticipantTotals implements IParticipantTotals {
    public _id:string;
    public points:number = 0;
    public pointsLastUpdated:string;
    public contacts:number = 0;
    public commitments:number = 0;
    public commitmentSales:number = 0;
    public purchases:number = 0;
    public purchasedSales:number = 0;
    public commitmentProfit:number = 0;
    public purchasedProfit:number = 0;

    constructor(participantTotalsJson?:IParticipantTotals) {
        if (participantTotalsJson) {
            this._id = participantTotalsJson._id;
            if (participantTotalsJson.hasOwnProperty("points")) {
                this.points = participantTotalsJson.points;
            }
            if (participantTotalsJson.hasOwnProperty("pointsLastUpdated")) {
                this.pointsLastUpdated = participantTotalsJson.pointsLastUpdated;
            }
            if (participantTotalsJson.hasOwnProperty("contacts")) {
                this.contacts = participantTotalsJson.contacts;
            }
            if (participantTotalsJson.hasOwnProperty("commitments")) {
                this.commitments = participantTotalsJson.commitments;
            }
            if (participantTotalsJson.hasOwnProperty("commitmentSales")) {
                this.commitmentSales = participantTotalsJson.commitmentSales;
            }
            if (participantTotalsJson.hasOwnProperty("purchases")) {
                this.purchases = participantTotalsJson.purchases;
            }
            if (participantTotalsJson.hasOwnProperty("purchasedSales")) {
                this.purchasedSales = participantTotalsJson.purchasedSales;
            }
            if (participantTotalsJson.hasOwnProperty("commitmentProfit")) {
                this.commitmentProfit = participantTotalsJson.commitmentProfit;
            }
            if (participantTotalsJson.hasOwnProperty("purchasedProfit")) {
                this.purchasedProfit = participantTotalsJson.purchasedProfit;
            }
        }
    }
}