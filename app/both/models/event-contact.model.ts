import * as moment from 'moment';
import {EventPoints, IEventPoints} from "./program-info.model";
import {IMyListContact} from "./my-list-contact.model";

export interface IEventContact {
    _id?:string,
    userId?:string,
    programId?:string,
    contactId?:string,
    status?:string,
    product?:string,
    commitment?:number,
    purchased?:number,
    price?:number,
    created?:string,
    updated?:string,
    called?:boolean,
    didAnswer?:boolean,
    leftMessage?:boolean,
    sentSMS?:boolean,
    receivedCommitment?:boolean,
    declinedCommitment?:boolean,
    sentOrderConfirmation?:boolean,
    isPaid?:boolean,
    paidOnline?:boolean,
    eventPoints?:IEventPoints,
    contact?:IMyListContact,
    purchases?:Array<IPurchase>,
    notes?:string
}

export class EventContact implements IEventContact {
    public _id:string;
    public userId:string;
    public programId:string;
    public contactId:string;
    public status:string;
    public product:string;
    public commitment:number;
    public purchased:number;
    public price:number;
    public created:string = moment().toISOString();
    public updated:string = moment().toISOString();
    public called:boolean;
    public didAnswer:boolean;
    public leftMessage:boolean;
    public sentSMS:boolean;
    public receivedCommitment:boolean;
    public declinedCommitment:boolean;
    public sentOrderConfirmation:boolean;
    public isPaid:boolean;
    public paidOnline:boolean;
    public eventPoints:EventPoints = new EventPoints();
    public contact:IMyListContact;
    public purchases:Array<IPurchase>;
    public notes:string;

    constructor(eventContactJson?:IEventContact) {
        if (eventContactJson) {
            this._id = eventContactJson._id;
            this.userId = eventContactJson.userId;
            this.programId = eventContactJson.programId;
            this.contactId = eventContactJson.contactId;
            this.status = eventContactJson.status;
            this.commitment = eventContactJson.commitment;
            this.purchased = eventContactJson.purchased;
            this.price = eventContactJson.price;
            this.called = eventContactJson.called;
            this.didAnswer = eventContactJson.didAnswer;
            this.leftMessage = eventContactJson.leftMessage;
            this.sentSMS = eventContactJson.sentSMS;
            this.receivedCommitment = eventContactJson.receivedCommitment;
            this.declinedCommitment = eventContactJson.declinedCommitment;
            this.sentOrderConfirmation = eventContactJson.sentOrderConfirmation;
            this.isPaid = eventContactJson.isPaid;
            this.paidOnline = eventContactJson.paidOnline;
            this.product = eventContactJson.product;
            if (eventContactJson.hasOwnProperty("created")) {
                this.created = eventContactJson.created;
            }
            if (eventContactJson.hasOwnProperty("updated")) {
                this.updated = eventContactJson.updated;
            }
            if (eventContactJson.hasOwnProperty("eventPoints")) {
                this.eventPoints = new EventPoints(eventContactJson.eventPoints);
            }
            if (eventContactJson.hasOwnProperty("contact")) {
                this.contact = eventContactJson.contact;
            }
            this.purchases = eventContactJson.purchases;
            this.notes = eventContactJson.notes;
        }
    }
}

export interface IPurchase {
    orderNumber?:number,
    productName?:string,
    quantity:number,
    price?:number
}