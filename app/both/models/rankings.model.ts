export interface IRankings {
    _id?:string,
    userId?:string,
    programId?:string,
    totalPoints?:number,
    lastUpdated?:string
}

export class Rankings implements IRankings {
    public _id:string;
    public userId: string;
    public programId:string;
    public totalPoints:number;
    public lastUpdated:string;
    
    constructor(rankingsJson?:IRankings) {
        if (rankingsJson) {
            this._id = rankingsJson._id;
            this.userId = rankingsJson.userId;
            this.programId = rankingsJson.programId;
            this.totalPoints = rankingsJson.totalPoints;
            this.lastUpdated = rankingsJson.lastUpdated;
        }
    }
}