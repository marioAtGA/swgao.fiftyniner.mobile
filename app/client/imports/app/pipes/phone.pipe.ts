import {Pipe, PipeTransform} from '@angular/core';
import {PhoneFormatter} from "../utils/PhoneFormatter";

@Pipe({
    name: 'phone'
})
export class PhonePipe implements PipeTransform {
    transform(tel:string, args:any) {
        return PhoneFormatter.transform(tel, args);
    }
}