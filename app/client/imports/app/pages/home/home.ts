import {Component, OnInit, NgZone, ViewChild} from '@angular/core';
import {App, NavController, Platform, Content} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {HomeTabPage} from "./tabs/home-tab/home-tab";
import {FollowUpsTabPage} from "./tabs/follow-ups-tab/follow-ups-tab";
import {RankingsTabPage} from "./tabs/rankings-tab/rankings-tab";
import {InfoTabPage} from "./tabs/info-tab/info-tab";
import {ContactsTabPage} from "./tabs/contacts-tab/contacts-tab";
import {ContactInfoPage} from "../event-contact/contact-info/contact-info";

@Component({
    selector: "page-home",
    templateUrl: "home.html"
})
export class HomePage extends MeteorComponent implements OnInit {
    @ViewChild(Content) content:Content;
    public user:Meteor.User;
    public tab1Root = HomeTabPage;
    public tab2Root = ContactInfoPage;
    public tab3Root = RankingsTabPage;
    public tab4Root = FollowUpsTabPage;
    public tab5Root = InfoTabPage;
    public isIPhoneX:boolean;

    constructor(public app:App,
                public platform:Platform,
                public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        // Use MeteorComponent autorun to respond to reactive session variables.
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();

            // Wait for translations to be ready
            // in case component loads before the language is set
            // or the language is changed after the component has been rendered.
            // Since this is the home page, this component and any child components
            // will need to wait for translations to be ready.
            if (Session.get(Constants.SESSION.TRANSLATIONS_READY)) {
                this.translate.get('page-home-tab.title').subscribe((translation:string) => {

                    // Set title of web page in browser
                    this.app.setTitle(translation);
                });
            }
            
            this.isIPhoneX = Session.get(Constants.SESSION.IS_IPHONE_X_LAYOUT);
        }));
    }
    
    private getOrientation():string {
        return this.platform.isPortrait() ? "portrait" : "landscape";
    }
}
