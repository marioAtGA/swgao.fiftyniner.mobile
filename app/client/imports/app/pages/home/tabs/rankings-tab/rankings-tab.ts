import {Component, OnInit, NgZone} from "@angular/core";
import {NavController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../../both/Constants";
import {IProgramInfo, IEventPoints} from "../../../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../../../both/collections/programs.collection";
import {IRankings, Rankings} from "../../../../../../../both/models/rankings.model";
import {RankingsCollection} from "../../../../../../../both/collections/rankings.collection";
import * as moment from "moment";
import {IProgramGroup} from "../../../../../../../both/models/program-group.model";
import {ProgramGroupsCollection} from "../../../../../../../both/collections/program-groups.collection";
import {ParticipantTotals} from "../../../../../../../both/models/participant-totals.model";
@Component({
    selector: "page-rankings-tab",
    templateUrl: "rankings-tab.html"
})
export class RankingsTabPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    private participants:Array<Meteor.User>;
    private rankings:Array<IRankings>;
    public participantRankings:Array<any>;
    public selectedSegment:string;
    public COMPETITION_TYPE:any = Constants.COMPETITION_TYPE;
    public programGroups:Array<IProgramGroup>;

    constructor(public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.autorun(() => {
            this.user = Meteor.user();
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
            this.selectedSegment = this.program.competitionType || Constants.COMPETITION_TYPE.INDIVIDUAL;
        });
        this.autorun(() => {
            var programId:string = Session.get(Constants.SESSION.PROGRAM_ID);
            if (programId) {
                // this.subscribe(Constants.PUBLICATIONS.RANKINGS, programId, () => {
                //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.RANKINGS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        // query aggregated collection
                        var rankings:Array<IRankings> = RankingsCollection.find({
                            programId: programId
                        }, {
                            sort: {
                                totalPoints: -1,
                                lastUpdated: 1
                            }
                        }).fetch();
                        console.log("rankings: " + rankings.length, rankings);
                        this.rankings = rankings;
                        this.joinParticipantsRankings();
                    }));
                // });

                // this.subscribe(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, programId, () => {
                //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        let queryKey:string = "roles." + programId;
                        let query:any = {};
                        query[queryKey] = {
                            $in: [Constants.ROLES.MEMBER]
                        };
                        var participants:Array<Meteor.User> = Meteor.users.find(query).fetch();
                        console.log("participants: " + participants.length, participants);
                        this.participants = participants;
                        this.joinParticipantsRankings();
                    }));
                // });

                // if (this.program.competitionType === Constants.COMPETITION_TYPE.GROUP) {
                //     this.subscribe(Constants.PUBLICATIONS.PROGRAM_GROUPS, this.program._id, () => {
                //         console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_GROUPS + " <><><>");
                        this.autorun(() => this.zone.run(() => {
                            var programGroups:Array<IProgramGroup> = ProgramGroupsCollection.find({
                                programId: this.program._id
                            }).fetch();
                            console.log("program groups: ", programGroups);
                            this.programGroups = programGroups;
                            this.joinParticipantsRankings();
                        }));
                //     });
                // }
            }
        });
    }

    private joinParticipantsRankings():void {
        var participantRankings:Array<any> = [];
        if (this.program && this.rankings && this.participants) {
            // this.participants.sort(this.sortParticipantsByRank.bind(this));

            this.participants.forEach((participant:Meteor.User, index:number) => {
                var participantRanking:IRankings = this.rankings.find(
                    (ranking:IRankings) => ranking.userId === participant._id);

                if (!participantRanking) {
                    participantRanking = {
                        totalPoints: 0
                    };
                }

                participant["ranking"] = new Rankings(participantRanking);
                
                let eventPoints:IEventPoints = participant.profile.programs[this.program._id].eventPoints;
                let points:number = Number(eventPoints.contactListGoal || 0) + 
                    Number(eventPoints.contactGoal || 0) + Number(eventPoints.salesGoalAward || 0);
                participant["ranking"].totalPoints += points;
                participant["ranking"].totalPoints += Number(this.program.eventPoints.registration || 0);
                if (!participant["ranking"].lastUpdated) {
                    participant["ranking"].lastUpdated = participant.profile.programs[this.program._id].eventPoints.updated;
                }

                participantRankings.push(participant);
            });
        }
        this.zone.run(() => {
            participantRankings.sort(this.sortParticipantsByTotalPoints.bind(this));
            this.participantRankings = participantRankings;
        });

        if (this.program.competitionType === Constants.COMPETITION_TYPE.GROUP) {
            this.groupParticipants();
        }
    }

    private groupParticipants():void {
        if (this.programGroups && this.participantRankings) {
            this.programGroups.forEach((group:IProgramGroup) => {
                group.members = [];
                group["totals"] = new ParticipantTotals();
            });
            this.participantRankings.forEach((participant:any) => {
                if (participant.profile.groups && participant.profile.groups[this.program._id]) {
                    let participantGroupId:string = participant.profile.groups[this.program._id];
                    let programGroup:any = this.programGroups.find((group:IProgramGroup) => {
                        return group._id === participantGroupId;
                    });
                    if (programGroup) {
                        if (participant.ranking) {
                            if (programGroup.members.length === 0) {
                                programGroup.totals.pointsLastUpdated = participant.ranking.lastUpdated;
                            } else {
                                let groupPointsLastUpdated = moment(programGroup.totals.pointsLastUpdated);
                                let participantPointsLastUpdated = moment(participant.ranking.lastUpdated);
                                if (groupPointsLastUpdated.diff(participantPointsLastUpdated) > 0) {
                                    programGroup.totals.pointsLastUpdated = participant.ranking.lastUpdated;
                                }
                            }

                            programGroup.totals.points += participant.ranking.totalPoints;
                        }

                        programGroup.members.push(participant);
                    }
                }
            });
            this.programGroups = this.programGroups.sort(this.sortProgramGroupsByTotalPoints.bind(this));
            this.zone.run(() => {
                this.programGroups.forEach((group:any, index:number) => {
                    group["rank"] = index + 1;
                });
            });
        }
    }

    private sortParticipantsByRank(a, b):number {
        var idMap:Array<string> = this.rankings.map((participant:IRankings) => {
            return participant._id;
        });
        var indexA = idMap.indexOf(a._id);
        var indexB = idMap.indexOf(b._id);
        if (indexA < indexB) {
            return 1;
        } else if (indexA > indexB) {
            return -1;
        } else {
            return 0;
        }
    }

    private sortParticipantsByTotalPoints(a, b):number {
        let sortDirection:number = b.ranking.totalPoints - a.ranking.totalPoints;
        if (sortDirection === 0) {
            let aMoment = moment(a.ranking.lastUpdated);
            let bMoment = moment(b.ranking.lastUpdated);
            if (aMoment.diff(bMoment) > 0) {
                sortDirection = 1;
            } else if (aMoment.diff(bMoment) < 0) {
                sortDirection = -1;
            }
        }
        return sortDirection;
    }

    private sortProgramGroupsByTotalPoints(a, b):number {
        let sortDirection:number = b.totals.points - a.totals.points;
        if (sortDirection === 0) {
            let aMoment = moment(a.totals.pointsLastUpdated);
            let bMoment = moment(b.totals.pointsLastUpdated);
            if (aMoment.diff(bMoment) > 0) {
                sortDirection = 1;
            } else if (aMoment.diff(bMoment) < 0) {
                sortDirection = -1;
            }
        }
        return sortDirection;
    }
}