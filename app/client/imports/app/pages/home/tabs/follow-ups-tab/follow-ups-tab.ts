import {Component, NgZone, OnInit} from "@angular/core";
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {IProgramInfo} from "../../../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../../../both/collections/programs.collection";
import {Constants} from "../../../../../../../both/Constants";
import {IMyListContact, MyListContact} from "../../../../../../../both/models/my-list-contact.model";
import {IEventContact} from "../../../../../../../both/models/event-contact.model";
import {GroundMyListContactsCollection} from "../../../../../../../both/collections/my-list-contacts";
import {GroundEventContactsCollection} from "../../../../../../../both/collections/event-contacts.collection";
import {OrderCommitmentPage} from "../../../event-contact/order-commitment/order-commitment";
import * as moment from "moment";
import {ContactInfoPage, IContactInfoPageParams} from "../../../event-contact/contact-info/contact-info";
import {ContactPage} from "../../../event-contact/contact/contact";

@Component({
    selector: "page-follow-ups-tab",
    templateUrl: "follow-ups-tab.html"
})

export class FollowUpsTabPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public program: IProgramInfo;
    private myListContacts: Array<IMyListContact>;
    private eventContacts: Array<IEventContact>;
    public filteredContacts: Array<IEventContact>;
    public SEGMENTS: any = {
        FOLLOW_UP: "follow-up",
        UNPAID: "unpaid",
        PAID: "paid",
        DECLINED: "declined"
    };
    public selectedSegment: string = this.SEGMENTS.FOLLOW_UP;
    public showAddActivityButton: boolean = false;

    constructor(public nav: NavController,
                public zone: NgZone,
                public translate: TranslateService) {
        super();
    }

    ngOnInit(): void {
        this.autorun(() => {
            this.user = Meteor.user();
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
            this.showAddActivityButton = !!this.program.startTime;
            var programId = this.program._id;
            if (this.user) {
                // this.subscribe(Constants.PUBLICATIONS.MY_LIST_CONTACTS, () => {
                //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_LIST_CONTACTS + " <><><>");
                this.autorun(() => this.zone.run(() => {
                    this.myListContacts = GroundMyListContactsCollection.find({
                        userId: this.user._id
                    }, {
                        sort: {
                            "name.family": 1,
                            "name.given": 1
                        }
                    }).fetch();
                    //console.log("myListContacts: ", this.myListContacts);
                    this.joinContactInfo();

                    let program: IProgramInfo = ProgramsCollection.findOne({
                        _id: programId
                    });
                    if (this.myListContacts.length >= program.contactListGoal) {
                        if (!this.user.profile.programs || !this.user.profile.programs[programId] || !this.user.profile.programs[programId].eventPoints ||
                            (!this.user.profile.programs[programId].eventPoints.contactListGoal &&
                                this.user.profile.programs[programId].eventPoints.contactListGoal !== 0)) {
                            let modifier: any = {$set: {}};
                            let modifierKeyString: string = "profile.programs." + programId + ".eventPoints.contactListGoal";
                            modifier.$set[modifierKeyString] = program.eventPoints.contactListGoal;
                            modifierKeyString = "profile.programs." + programId + ".eventPoints.updated";
                            modifier.$set[modifierKeyString] = moment().toISOString();
                            Meteor.users.update({_id: this.user._id}, modifier);
                        }
                    }
                }));
                // });

                // this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS, {
                //     programId: programId,
                //     participantId: this.user._id
                // }, () => {
                //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS + " <><><>");
                this.autorun(() => this.zone.run(() => {
                    this.eventContacts = GroundEventContactsCollection.find({
                        userId: this.user._id,
                        programId: programId
                    }).fetch();
                    //console.log("EventContacts: ", this.eventContacts);
                    this.joinContactInfo();
                }));
                // });
            }
        });
    }

    private joinContactInfo(): void {
        if (this.myListContacts && this.eventContacts) {
            this.eventContacts.forEach((eventContact: IEventContact) => {
                var myContact: IMyListContact = this.myListContacts.find(
                    (contact: IMyListContact) => contact._id === eventContact.contactId);
                eventContact["contact"] = new MyListContact(myContact);
            });
            this.filterContactsBySelectedSegment();
        }
    }

    private filterContactsBySelectedSegment(): void {
        if (this.myListContacts && this.eventContacts) {
            let followUpContacts: Array<IEventContact> = [];
            let committedContacts: Array<IEventContact> = [];
            let paidContacts: Array<IEventContact> = [];
            let declinedContacts: Array<IEventContact> = [];
            this.eventContacts.forEach((eventContact: IEventContact) => {
                if (eventContact.receivedCommitment) {
                    if (!eventContact.isPaid) {
                        committedContacts.push(eventContact);
                    } else {
                        paidContacts.push(eventContact);
                    }
                } else if (eventContact.declinedCommitment) {
                    declinedContacts.push(eventContact);
                } else {
                    followUpContacts.push(eventContact);
                }
            });
            switch (this.selectedSegment) {
                case this.SEGMENTS.FOLLOW_UP:
                    this.filteredContacts = followUpContacts;
                    break;
                case this.SEGMENTS.UNPAID:
                    this.filteredContacts = committedContacts;
                    break;
                case this.SEGMENTS.PAID:
                    this.filteredContacts = paidContacts;
                    break;
                case this.SEGMENTS.DECLINED:
                    this.filteredContacts = declinedContacts;
                    break;
            }
        }
    }

    private onSegmentChanged(): void {
        this.filterContactsBySelectedSegment();
    }

    private editCommitment(eventContact: IEventContact): void {
        this.nav.push(OrderCommitmentPage, eventContact);
    }

    private addActivity(): void {
        let params: IContactInfoPageParams = {
            method: Constants.EVENT_CONTACT_METHOD.CALL,
            addActivity: true
        };
        this.nav.push(ContactInfoPage, params);
    }

    private goContact(eventContact: IEventContact) {
        this.nav.push(ContactPage, {
            method: Constants.EVENT_CONTACT_METHOD.CALL,
            contact: eventContact.contact
        });
    }
}