import {Component, OnInit, NgZone} from "@angular/core";
import {NavController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {IProgramInfo, IEventPoints} from "../../../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../../../both/collections/programs.collection";
import {Constants} from "../../../../../../../both/Constants";
import {IEventContact, IPurchase} from "../../../../../../../both/models/event-contact.model";
import {GroundEventContactsCollection} from "../../../../../../../both/collections/event-contacts.collection";
import {AwardProgressHelper} from "../../../../utils/AwardProgressHelper";
import {IAwardInfo, AwardInfo} from "../../../../../../../both/models/award-info.model";
declare var Circles;
@Component({
    selector: "page-info-tab",
    templateUrl: "info-tab.html"
})
export class InfoTabPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public programEventPoints:IEventPoints;
    public awards:Array<IAwardInfo> = [];

    constructor(public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.autorun(() => {
            this.user = Meteor.user();
            let programId:string = Session.get(Constants.SESSION.PROGRAM_ID);
            this.program = ProgramsCollection.findOne({_id: programId});
            if (programId) {
                this.autorun(() => this.zone.run(() => {
                    let eventContacts:Array<IEventContact> = GroundEventContactsCollection.find({
                        userId: this.user._id,
                        programId: programId
                    }).fetch();

                    let totals:any = {
                        calls: 0,
                        texts: 0,
                        commitments: 0,
                        purchased: 0,
                        points: {
                            call: 0,
                            text: 0,
                            commitment: 0,
                            paid: 0
                        },
                        sales: {
                            commitment: 0,
                            purchased: 0
                        },
                        totalPoints: 0
                    };
                    eventContacts.forEach((eventContact:IEventContact) => {
                        if (eventContact.called) {
                            totals.calls++;
                        }
                        if (eventContact.sentSMS) {
                            totals.texts++;
                        }
                        if (eventContact.commitment) {
                            totals.commitments += eventContact.commitment;
                            totals.sales.commitment += (eventContact.commitment * eventContact.price);
                        }
                        if (eventContact.purchased) {
                            totals.purchased += eventContact.purchased;
                            eventContact.purchases.forEach((purchase:IPurchase) => {
                                totals.sales.purchased += purchase.price;
                            });
                        }

                        // Points
                        if (eventContact.eventPoints.call) {
                            totals.points.call += eventContact.eventPoints.call;
                        }
                        if (eventContact.eventPoints.text) {
                            totals.points.text += eventContact.eventPoints.text;
                        }
                        if (eventContact.eventPoints.commitment) {
                            totals.points.commitment += eventContact.eventPoints.commitment;
                        }
                        if (eventContact.eventPoints.paid) {
                            totals.points.paid += eventContact.eventPoints.paid;
                        }
                    });
                    totals.totalPoints += (totals.points.call + totals.points.text + totals.points.commitment + totals.points.paid);
                    let eventPoints:IEventPoints = this.user.profile.programs[this.program._id].eventPoints;
                    this.programEventPoints = eventPoints;
                    totals.totalPoints += (Number(eventPoints.contactListGoal || 0) +
                    Number(eventPoints.contactGoal || 0) +
                    Number(eventPoints.salesGoalAward || 0) +
                    Number(this.program.eventPoints.registration || 0));

                    if (this.program.contract.profitPercent) {
                        if (totals.sales.commitment) {
                            totals.sales.commitmentProfit = totals.sales.commitment * this.program.contract.profitPercent;
                        }
                        if (totals.sales.purchased) {
                            totals.sales.purchasedProfit = totals.sales.purchased * this.program.contract.profitPercent;
                        }
                    }

                    this.user["totals"] = totals;
                    this.updateAwards();
                }));
            }
        });
    }

    ionViewWillEnter() {
        this.updateAwards();
    }

    private updateAwards():void {
        let awardProgressHelper:AwardProgressHelper = new AwardProgressHelper(this.user, this.program);
        this.awards = [];
        (Object as any).values(awardProgressHelper.awardInfos).forEach((award:IAwardInfo) => {
            this.awards.push(new AwardInfo(award));
        });
    }
}