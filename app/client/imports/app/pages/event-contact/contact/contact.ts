import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, AlertController, ModalController} from "ionic-angular/es2015";
import {Sim} from "@ionic-native/sim";
import {SMS} from "@ionic-native/sms";
import {EmailComposer, EmailComposerOptions} from "@ionic-native/email-composer";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {IProgramInfo} from "../../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../../both/collections/programs.collection";
import {Constants} from "../../../../../../both/Constants";
import {IEventContact, EventContact} from "../../../../../../both/models/event-contact.model";
import {GroundEventContactsCollection} from "../../../../../../both/collections/event-contacts.collection";
import {ToastMessenger} from "../../../utils/ToastMessenger";
import {IMyListContact} from "../../../../../../both/models/my-list-contact.model";
import {OrderCommitmentPage} from "../order-commitment/order-commitment";
import {PhoneFormatter} from "../../../utils/PhoneFormatter";
import {ClientMethods} from "../../../utils/ClientMethods";

declare var device; // cordova-plugin-device

@Component({
    selector: "page-contact",
    templateUrl: "contact.html"
})
export class ContactPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public contact:IMyListContact;
    public eventContact:EventContact;
    public CONTACT_METHOD:any = Constants.EVENT_CONTACT_METHOD;
    public callScript:string = null;
    public textScript:string = null;
    public customMessage:string;
    private myPhoneNumber:string = Constants.EMPTY_STRING;
    private enterOrderCommitment:boolean = false;
    private paymentLink:string = Constants.EMPTY_STRING;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private sim:Sim,
                private sms:SMS,
                private emailComposer:EmailComposer) {
        super();
    }

    ngOnInit():void {
        this.contact = this.params.data.contact;
        this.autorun(() => {
            this.user = Meteor.user();
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
            this.initEventContact();
            this.getSimInfo();
            this.setContactScripts();
        });
    }

    private getSimInfo():void {
        let self = this;
        if (Meteor.isCordova) {
            if (device.platform === Constants.DEVICE.ANDROID) {
                self.sim.hasReadPermission().then((hasPermission:boolean) => {
                    console.log("Has permission to read SIM info: " + hasPermission);
                    if (!hasPermission) {
                        self.requestSimPermission();
                    } else {
                        self.sim.getSimInfo().then(
                            self.readSimInfo.bind(self),
                            self.getSimInfoError.bind(self)
                        );
                    }
                });
            } else {
                self.sim.getSimInfo().then(
                    self.readSimInfo.bind(self),
                    self.getSimInfoError.bind(self)
                );
            }
        }
    }

    private requestSimPermission():void {
        let self = this;
        if (Meteor.isCordova) {
            self.sim.requestReadPermission().then(
                self.simPermissionGranted.bind(self),
                self.simPermissionDenied.bind(self)
            );
        }
    }

    private simPermissionGranted():void {
        console.log('Permission granted');
        this.getSimInfo();
    }

    private simPermissionDenied():void {
        let self = this;
        console.log('Permission denied');
        let alert = self.alertCtrl.create({
            title: self.translate.instant("general.permissionDenied"),
            message: self.translate.instant("page-contact.alerts.simPermissionDenied.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.tryAgain"),
                handler: () => {
                    self.requestSimPermission();
                }
            }]
        });
        alert.present();
    }

    private readSimInfo(info:any):void {
        console.log("SIM Info: " + JSON.stringify(info));
        this.myPhoneNumber = info['phoneNumber'];
        this.setContactScripts();
    }

    private getSimInfoError(error:any):void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.errors.simInfo"),
            message: JSON.stringify(error),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.tryAgain"),
                handler: () => {
                    self.getSimInfo();
                }
            }]
        });
        alert.present();
    }

    private setContactScripts():void {
        if (this.user && this.program && this.contact) {
            let callScript:string = this.program.scripts.call;
            let textScript:string = this.program.scripts.text;

            this.zone.run(() => {
                this.callScript = this.replaceScriptVariables(callScript);
                this.textScript = this.replaceScriptVariables(textScript);
            });
        }
    }

    private shortenPaymentLink():void {
        let self = this;
        if (this.user && this.program && this.eventContact) {
            this.paymentLink = ClientMethods.getPaymentLink({
                user: this.user,
                program: this.program,
                eventContact: this.eventContact
            });
            if (!Meteor.status().connected) {
                self.setContactScripts();
            } else {
                Session.set(Constants.SESSION.LOADING, true);
                Meteor.call("shortenUrl", this.paymentLink, (error, result) => {
                    Session.set(Constants.SESSION.LOADING, false);
                    if (error) {
                        console.error("Shorten Payment Link Error: ", error);
                    } else {
                        if (result.data && result.data.shortLink) {
                            this.paymentLink = result.data.shortLink;
                        }
                    }
                    self.setContactScripts();
                });
            }
        }
    }

    private replaceScriptVariables(script:string):string {
        if (!this.paymentLink && script.includes("{{paymentLink}}")) {
            this.shortenPaymentLink();
        }
        script = script.replace("{{programName}}", this.program.name);
        script = script.replace("{{duration}}", this.program.duration.toString());
        script = script.replace("{{salesGoal}}", this.program.salesGoal.toString());
        script = script.replace("{{productName}}", this.program.contract.product.label || "_____");
        script = script.replace("{{contactName}}", this.contact.name.given);
        let formattedPhoneNumber:string = PhoneFormatter.transform(this.myPhoneNumber || this.user.username);
        script = script.replace("{{myPhoneNumber}}", formattedPhoneNumber);
        script = script.replace("{{me}}", this.user.profile.name.display);
        script = script.replace("{{paymentLink}}", this.paymentLink);
        return script;
    }

    private onSubmit(contactMethod:string):void {
        let self = this;
        // if (Meteor.isCordova) {
        switch (contactMethod) {
            case Constants.EVENT_CONTACT_METHOD.CALL:
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-contact.alerts.calling.title") + " " + self.contact.name.display,
                    message: self.callScript,
                    buttons: [{
                        text: self.translate.instant("general.cancel"),
                        role: 'cancel'
                    }, {
                        text: self.translate.instant("general.done"),
                        handler: () => {
                            self.startCallLog();
                        }
                    }],
                    enableBackdropDismiss: false
                });
                alert.present();
                break;
            case Constants.EVENT_CONTACT_METHOD.TEXT:
                if (Meteor.isCordova) {
                    self.startSendingSMS();
                }
                break;
            case Constants.EVENT_CONTACT_METHOD.EMAIL:
                if (Meteor.isCordova) {
                    self.emailComposer.isAvailable().then(
                        self.emailComposerAvailableSuccess.bind(self),
                        self.emailComposerAvailableError.bind(self)
                    );
                }
                break;
        }
        // }
    }

    /**
     * SMS Methods
     * */
    private startSendingSMS():void {
        let self = this;
        if (device.platform === Constants.DEVICE.ANDROID) {
            self.sms.hasPermission().then(
                self.smsHasPermissionSuccess.bind(self),
                self.smsHasPermissionError.bind(self)
            );
        } else {
            self.sendSMS()
        }
    }

    private smsHasPermissionSuccess():void {
        console.log("smsHasPermissionSuccess()");
        this.sendSMS();
    }

    private smsHasPermissionError(error:any):void {
        let self = this;
        console.log("smsHasPermissionError(): " + JSON.stringify(error));
        let alert = self.alertCtrl.create({
            title: self.translate.instant("general.permissionDenied"),
            message: self.translate.instant("page-contact.alerts.smsPermissionDenied.message"),
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    private sendSMS():void {
        let self = this;
        Session.set(Constants.SESSION.LOADING, true);
        let message:string = self.textScript;
        if (self.customMessage) {
            message = self.customMessage
        }
        self.sms.send(self.contact.phone, message, {replaceLineBreaks: true}).then(
            self.smsSuccess.bind(self),
            self.smsError.bind(self)
        );
    }

    private smsSuccess():void {
        let self = this;
        console.log("Successfully sent SMS message.");
        Session.set(Constants.SESSION.LOADING, false);
        self.initEventContact();
        self.eventContact.sentSMS = true;
        self.eventContact.status = Constants.EVENT_CONTACT_STATUS.SENT_TEXT;
        self.eventContact.eventPoints.text = self.program.eventPoints.text;
        self.logEventContact(() => {
            self.logEventContactSuccess(Constants.EVENT_CONTACT_METHOD.TEXT);
        });
    }

    private smsError(error):void {
        let self = this;
        console.log("SMS Error: " + JSON.stringify(error));
        Session.set(Constants.SESSION.LOADING, false);
        let alertOptions:any = {
            title: self.translate.instant("page-contact.errors.sms"),
            message: JSON.stringify(error),
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        };

        if (error === Constants.METEOR_ERRORS.PERMISSION_DENIED) {
            alertOptions.title = self.translate.instant("general.permissionDenied");
            alertOptions.message = self.translate.instant("page-contact.alerts.smsPermissionDenied.message");
        }

        if (error !== "Message cancelled.") {
            let alert = self.alertCtrl.create(alertOptions);
            alert.present();
        }
    }

    /*End SMS Methods*/

    /**
     * Email Composer Methods
     * */
    private emailComposerAvailableSuccess(available:boolean):void {
        let self = this;
        console.log("emailComposerAvailableSuccess()");
        var message:string = self.textScript;
        if (self.customMessage) {
            message = self.customMessage
        }
        let email:EmailComposerOptions = {
            to: self.contact.email,
            subject: self.program.name + " - Support Our Fundraiser",
            body: message,
            isHtml: false
        };
        self.emailComposer.open(email).then(
            self.sendEmailSuccess.bind(self),
            self.sendEmailError.bind(self)
        );
    }

    private emailComposerAvailableError(error:any):void {
        let self = this;
        console.log("emailComposerAvailableError(): " + JSON.stringify(error));
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.errors.composeEmail"),
            subTitle: self.translate.instant("page-contact.alerts.emailAvailableError.subTitle"),
            message: JSON.stringify(error),
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    private sendEmailSuccess():void {
        let self = this;
        console.log("Sent email");
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.alerts.emailConfirmation.title"),
            message: self.translate.instant("page-contact.alerts.emailConfirmation.message", {
                contactName: self.contact.name.display
            }),
            buttons: [{
                text: self.translate.instant("general.no"),
                role: "cancel"
            }, {
                text: self.translate.instant("general.yes"),
                handler: () => {
                    self.logEventContact(() => {
                        self.logEventContactSuccess(Constants.EVENT_CONTACT_METHOD.EMAIL);
                    });
                }
            }]
        });
        alert.present();
    }

    private sendEmailError(error:any):void {
        let self = this;
        console.log("Error sending email: " + JSON.stringify(error));
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.errors.composeEmail"),
            subTitle: self.translate.instant("page-contact.alerts.sendEmailError.subTitle"),
            message: JSON.stringify(error),
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    /*End Email Composer Methods*/


    private startCallLog():void {
        let self = this;
        self.initEventContact();
        self.eventContact.called = true;
        self.eventContact.status = Constants.EVENT_CONTACT_STATUS.CALLED;
        self.eventContact.eventPoints.call = self.program.eventPoints.call;
        self.inquireCommitment();
        // let alert = self.alertCtrl.create({
        //     title: self.translate.instant("page-contact.alerts.callLog.title"),
        //     subTitle: self.translate.instant("page-contact.alerts.callLog.receiveAnswer"),
        //     buttons: [{
        //         text: self.translate.instant("general.no"),
        //         role: 'cancel',
        //         handler: () => {
        //             self.eventContact.didAnswer = false;
        //             self.inquireVoiceMessage();
        //         }
        //     }, {
        //         text: self.translate.instant("general.yes"),
        //         handler: () => {
        //             self.eventContact.didAnswer = true;
        //             console.log("Open next dialog");
        //             self.inquireCommitment();
        //         }
        //     }],
        //     enableBackdropDismiss: false
        // });
        // alert.present();
    }

    private inquireCommitment():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.alerts.callLog.title"),
            subTitle: self.translate.instant("page-contact.alerts.callLog.receiveCommitment"),
            buttons: [{
                text: self.translate.instant("general.no"),
                role: 'cancel',
                handler: () => {
                    // self.eventContact.declinedCommitment = true;
                    // self.eventContact.status = Constants.EVENT_CONTACT_STATUS.DECLINED;
                    self.logEventContact(() => {
                        self.logEventContactSuccess(Constants.EVENT_CONTACT_METHOD.CALL);
                    });
                }
            }, {
                text: self.translate.instant("general.yes"),
                handler: () => {
                    self.enterOrderCommitment = true;
                    self.eventContact.receivedCommitment = true;
                    self.eventContact.status = Constants.EVENT_CONTACT_STATUS.RECEIVED_COMMITMENT;
                    self.logEventContact(() => {
                        self.logEventContactSuccess(Constants.EVENT_CONTACT_METHOD.CALL);
                    });
                }
            }],
            enableBackdropDismiss: false
        });
        alert.present();
    }

    private inquireVoiceMessage():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.alerts.callLog.title"),
            subTitle: self.translate.instant("page-contact.alerts.callLog.voiceMessage"),
            buttons: [{
                text: self.translate.instant("general.no"),
                role: 'cancel',
                handler: () => {
                    self.eventContact.leftMessage = false;
                    self.inquireSMS();
                }
            }, {
                text: self.translate.instant("general.yes"),
                handler: () => {
                    self.eventContact.status = Constants.EVENT_CONTACT_STATUS.LEFT_MESSAGE;
                    self.eventContact.leftMessage = true;
                    self.inquireSMS();
                }
            }],
            enableBackdropDismiss: false
        });
        alert.present();
    }

    private inquireSMS():void {
        let self = this;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-contact.alerts.callLog.title"),
            subTitle: self.translate.instant("page-contact.alerts.callLog.sendText"),
            buttons: [{
                text: self.translate.instant("general.no"),
                role: 'cancel',
                handler: () => {
                    self.logEventContact(() => {
                        self.logEventContactSuccess(Constants.EVENT_CONTACT_METHOD.CALL);
                    });
                }
            }, {
                text: self.translate.instant("general.yes"),
                handler: () => {
                    self.logEventContact(() => {
                        self.startSendingSMS();
                    });
                }
            }],
            enableBackdropDismiss: false
        });
        alert.present();
    }

    private initEventContact() {
        if (!this.eventContact) {
            let eventContact:IEventContact = GroundEventContactsCollection.findOne({
                userId: this.user._id,
                programId: this.program._id,
                contactId: this.contact._id
            });

            if (!eventContact) {
                eventContact = {
                    _id: new Mongo.ObjectID().toHexString(),
                    userId: this.user._id,
                    programId: this.program._id,
                    contactId: this.contact._id
                };
            }

            this.eventContact = new EventContact(eventContact);
        }
    }

    private logEventContact(callback:Function):void {
        let self = this;
        ClientMethods.logEventContact(self.eventContact, (error, result) => {
            if (error) {
                console.log("logEventContact() Error: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-contact.errors.logEventContact"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.cancel"),
                        role: 'cancel'
                    }, {
                        text: self.translate.instant("general.tryAgain"),
                        handler: () => {
                            self.logEventContact(callback);
                        }
                    }]
                });
                alert.present();
            } else {
                if (result && result.insertedId) {
                    this.eventContact._id = result.insertedId;
                }
                callback();
            }
        });
        // Meteor.call("logEventContact", self.eventContact, (error, result) => {
        //     if (error) {
        //         console.log("logEventContact() Error: " + JSON.stringify(error));
        //         let alert = self.alertCtrl.create({
        //             title: self.translate.instant("page-contact.errors.logEventContact"),
        //             message: error.reason || error.message || error,
        //             buttons: [{
        //                 text: self.translate.instant("general.cancel"),
        //                 role: 'cancel'
        //             }, {
        //                 text: self.translate.instant("general.tryAgain"),
        //                 handler: () => {
        //                     self.logEventContact(callback);
        //                 }
        //             }]
        //         });
        //         alert.present();
        //     } else {
        //         if (result && result.insertedId) {
        //             this.eventContact._id = result.insertedId;
        //         }
        //         callback();
        //     }
        // });
    }

    private logEventContactSuccess(contactMethod:string):void {
        let self = this;
        let message = self.translate.instant("page-contacts-tab.labels.text") +
            " " + self.translate.instant("general.sent");
        if (contactMethod === Constants.EVENT_CONTACT_METHOD.CALL) {
            message = self.translate.instant("page-contacts-tab.labels." + contactMethod) +
                " " + self.translate.instant("general.completed");
        }
        new ToastMessenger().toast({
            type: "success",
            message: message
        });
        Session.set(Constants.SESSION.CONTACT_COMPLETE, true);
        if (self.enterOrderCommitment) {
            self.nav.pop().then(() => {
                self.eventContact.contact = self.contact;
                self.nav.push(OrderCommitmentPage, self.eventContact);
            });
        } else {
            if (self.nav.canGoBack()) {
                self.nav.pop();
            }
        }
        ClientMethods.checkIfEarnedAward(Constants.AWARD_KEYS.COMMUNICATION, this.modalCtrl);
    }
}