import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, AlertController, ModalController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {Contacts, Contact} from "@ionic-native/contacts";
import {IProgramInfo} from "../../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../../both/collections/programs.collection";
import {Constants} from "../../../../../../both/Constants";
import {FormValidator} from "../../../utils/FormValidator";
import {ContactPage} from "../contact/contact";
import {IMyListContact, MyListContact} from "../../../../../../both/models/my-list-contact.model";
import {GroundMyListContactsCollection} from "../../../../../../both/collections/my-list-contacts";
import {ToastMessenger} from "../../../utils/ToastMessenger";
import {MyContactListPage} from "../my-contact-list/my-contact-list";
import {Utils} from "../../../../../../both/Utils";
import {OrderCommitmentPage} from "../order-commitment/order-commitment";
import {EventContact, IEventContact} from "../../../../../../both/models/event-contact.model";
import {GroundEventContactsCollection} from "../../../../../../both/collections/event-contacts.collection";
import {ClientMethods} from "../../../utils/ClientMethods";
declare var ContactError; // cordova-plugin-contacts


@Component({
    selector: "page-contact-info",
    templateUrl: "contact-info.html"
})
export class ContactInfoPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public contactMethod:string = Constants.EVENT_CONTACT_METHOD.CALL;
    public formGroup:FormGroup;
    public formControl:{
        givenName:AbstractControl,
        familyName:AbstractControl,
        phone?:AbstractControl
        email?:AbstractControl
    };
    public contact:MyListContact;
    public CONTACT_METHOD:any = Constants.EVENT_CONTACT_METHOD;
    private contactInfoPageParams:IContactInfoPageParams;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder,
                private contactsPlugin:Contacts) {
        super();
    }

    ngOnInit():void {
        this.contactInfoPageParams = this.params.data;
        //console.log("params: ", this.params);
        //console.log("contactInfoPageParams: ", this.contactInfoPageParams);
        this.contact = new MyListContact(this.contactInfoPageParams.contact);

        if (this.contactInfoPageParams.method) {
            this.contactMethod = this.contactInfoPageParams.method;
        }

        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
        }));

        var phoneControl:any = [Constants.EMPTY_STRING, Validators.compose([
            Validators.required,
            Validators.minLength(7),
            Validators.maxLength(11),
            FormValidator.notRegistered,
            FormValidator.validPhoneLength
        ])];

        var emailControl:any = [Constants.EMPTY_STRING, Validators.compose([
            Validators.required,
            FormValidator.validEmail
        ])];

        var formGroup:any = {
            'givenName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])],
            'familyName': [Constants.EMPTY_STRING, Validators.compose([
            ])]
        };

        switch (this.contactMethod) {
            case Constants.EVENT_CONTACT_METHOD.CALL:
            case Constants.EVENT_CONTACT_METHOD.TEXT:
                formGroup.phone = phoneControl;
                break;
            case Constants.EVENT_CONTACT_METHOD.EMAIL:
                formGroup.email = emailControl;
                break;
        }

        this.formGroup = this.fb.group(formGroup);
        this.formControl = {
            givenName: this.formGroup.controls['givenName'],
            familyName: this.formGroup.controls['familyName']
        };

        switch (this.contactMethod) {
            case Constants.EVENT_CONTACT_METHOD.CALL:
            case Constants.EVENT_CONTACT_METHOD.TEXT:
                this.formControl.phone = this.formGroup.controls['phone'];
                break;
            case Constants.EVENT_CONTACT_METHOD.EMAIL:
                this.formControl.email = this.formGroup.controls['email'];
                break;
        }
    }

    private onPhoneNumberChanged():void {
        this.zone.run(() => {
            this.contact.phone = Utils.removeNonDigits(this.contact.phone);
            if (this.contactInfoPageParams.contact && this.contactInfoPageParams.contact._id) {
                if (this.contactInfoPageParams.contact.phone !== this.contact.phone) {
                    //console.log("Phone number changed, clearing contact _id.");
                    this.contact._id = Constants.EMPTY_STRING;
                } else {
                    //console.log("New input equals old phone number.");
                    this.contact._id = this.contactInfoPageParams.contact._id;
                }
            }
        });
    }

    private onNameChanged():void {
        this.contact.name.display = this.contact.name.given + " " + this.contact.name.family;
    }

    private pickContact():void {
        var self = this;
        if (Meteor.isCordova) {
            self.contactsPlugin.pickContact().then(function (selectedContact:Contact) {
                //console.log('The following contact has been selected:' + JSON.stringify(selectedContact));

                let phoneNumber:string;
                if (selectedContact.phoneNumbers && selectedContact.phoneNumbers.length > 0) {
                    if (selectedContact.phoneNumbers.length > 1) {
                        let phoneType:any = {
                            main: 1,
                            iphone: 2,
                            mobile: 2,
                            home: 4,
                            work: 5,
                            other: 6
                        };
                        selectedContact.phoneNumbers.sort(function (a, b) {
                            let aType = phoneType[a.type] || 7;
                            let bType = phoneType[b.type] || 7;
                            return aType - bType;
                        });
                    }
                    phoneNumber = selectedContact.phoneNumbers[0].value
                }
                phoneNumber = Utils.removeNonDigits(phoneNumber);

                // let email:string;
                // if (selectedContact.emails && selectedContact.emails.length > 0) {
                //     email = selectedContact.emails[0].value;
                // }
                let displayName:string = selectedContact.name.givenName + " " + selectedContact.name.familyName;
                let contact:MyListContact = new MyListContact({
                    name: {
                        given: selectedContact.name.givenName.trim(),
                        family: selectedContact.name.familyName.trim(),
                        display: displayName.trim()
                    },
                    phone: phoneNumber
                    // email: email
                });

                self.zone.run(() => {
                    self.contact = contact;

                    Object.keys(self.formControl).forEach((key) => {
                        self.formControl[key].updateValueAndValidity();
                        self.formControl[key].markAsTouched();
                    });

                    self.goContact();
                });
            }, function (err) {
                //console.log('pickContact() Error: ' + JSON.stringify(err));
                if (err !== ContactError.OPERATION_CANCELLED_ERROR) {
                    var alertOptions:any = {
                        message: self.translate.instant("page-contact-info.errors.retrieveContactInfo"),
                        buttons: [self.translate.instant("general.ok")]
                    };
                    if (err === ContactError.PERMISSION_DENIED_ERROR) {
                        alertOptions.title = self.translate.instant("general.permissionDenied")
                    }
                    let alert = self.alertCtrl.create(alertOptions);
                    alert.present();
                }
            });
        }
    }

    private pickFromList():void {
        this.nav.push(MyContactListPage, {
            pickFromList: true,
            contactInfoPageParams: this.contactInfoPageParams
        });
    }

    private goContact():void {
        var self = this;
        if (self.formGroup.valid) {
            var existingContact:IMyListContact = GroundMyListContactsCollection.findOne({phone: self.contact.phone});

            if (!self.contact._id) {
                if (existingContact) {
                    //console.log("Update existing contact...");
                    self.contact._id = existingContact._id;
                }
                self.saveContactInfo(() => {
                    self.goNextPage();
                });
            } else {
                if (self.contactInfoPageParams.contact && self.contactInfoPageParams.contact.name.display) {
                    if (self.contactInfoPageParams.contact.name.display !== self.contact.name.display) {
                        self.saveContactInfo(() => {
                            self.nav.push(ContactPage, {
                                method: self.contactMethod,
                                contact: self.contact
                            });
                        });
                        return;
                    }
                }

                self.goNextPage();
            }
        }
    }

    private goNextPage():void {
        var self = this;
        if (!this.contactInfoPageParams.addActivity) {
            self.nav.push(ContactPage, {
                method: self.contactMethod,
                contact: self.contact
            });
        } else {
            let eventContact:EventContact = self.getEventContact();
            self.nav.push(OrderCommitmentPage, eventContact);
        }
    }

    private getEventContact():EventContact {
        var eventContact:IEventContact = GroundEventContactsCollection.findOne({
            userId: this.user._id,
            programId: this.program._id,
            contactId: this.contact._id
        });

        if (!eventContact) {
            eventContact = {
                userId: this.user._id,
                programId: this.program._id,
                contactId: this.contact._id
            };
        }

        eventContact.contact = this.contact;
        return new EventContact(eventContact);
    }

    private onSaveContactInfo():void {
        var self = this;
        if (this.formGroup.valid) {
            var existingContact:IMyListContact = GroundMyListContactsCollection.findOne({phone: this.contact.phone});

            if (existingContact && existingContact._id !== this.contact._id) {
                if (!existingContact.hidden) {
                    Session.set(Constants.SESSION.NOT_REGISTERED_ERROR, true);
                    self.formControl.phone.updateValueAndValidity({onlySelf: true});
                    return new ToastMessenger().toast({
                        type: "error",
                        message: self.translate.instant("page-contact-info.toast.exists")
                    });
                } else {
                    //console.log("Found hidden contact");
                    existingContact.name = this.contact.name;
                    existingContact.hidden = false;
                    this.contact = new MyListContact(existingContact);
                }
            }

            self.saveContactInfo(() => {
                new ToastMessenger().toast({
                    type: "success",
                    message: self.translate.instant("page-contact-info.toast.savedContact")
                });
                self.nav.pop();
            });
        }
    }

    private saveContactInfo(callback:Function):void {
        var self = this;
        if (!callback) {
            return;
        }
        this.contact.userId = this.user._id;

        ClientMethods.saveContactInfo(this.contact, (error, result) => {
            self.saveContactInfoCallback({
                callback: callback,
                error: error,
                result: result
            });
        });
    }

    private saveContactInfoCallback(data:{callback:Function, error?:any, result?:any}):void {
        var self = this;
        if (data.error) {
            console.error("saveContactInfo() Error: " + JSON.stringify(data.error));
            let alert = self.alertCtrl.create({
                title: self.translate.instant("page-contact-info.errors.saveContactInfo"),
                message: data.error.reason || data.error.message || data.error,
                buttons: [
                    // {
                    //     text: self.translate.instant("general.cancel"),
                    //     role: 'cancel'
                    // },
                    {
                        text: self.translate.instant("general.ok")
                        // handler: () => {
                        //     self.saveContactInfo(data.callback);
                        // }
                    }]
            });
            alert.present();
        } else {
            //console.log("saveContactInfo() Result: " + JSON.stringify(data.result));
            if (data.result && data.result.insertedId) {
                this.contact._id = data.result.insertedId;
            }
            data.callback();

            ClientMethods.checkIfEarnedAward(Constants.AWARD_KEYS.CONTACT_LIST, this.modalCtrl);
        }
    }

    ionViewWillEnter() {
        if (!this.program.startTime && !this.contactInfoPageParams.addToContactList) {
            let alert = this.alertCtrl.create({
                title: this.translate.instant("page-contact-info.alerts.eventDate.title"),
                message: this.translate.instant("page-contact-info.alerts.eventDate.message"),
                buttons: [{
                    text: this.translate.instant("page-home-tab.title"),
                    role: "cancel",
                    handler: () => {
                        this.nav.parent.select(0);
                    }
                }, {
                    text: this.translate.instant("page-my-contact-list.title"),
                    handler: () => {
                        this.nav.push(MyContactListPage);
                    }
                }],
                enableBackdropDismiss: false
            });
            alert.present();
        }

        if (Session.get(Constants.SESSION.CONTACT_COMPLETE)) {
            //console.log("Contact Complete!");
            Session.set(Constants.SESSION.CONTACT_COMPLETE, false);
            if (this.contactInfoPageParams.addActivity) {
                this.nav.pop();
            } else {
                this.contact = new MyListContact(this.contactInfoPageParams.contact);
                if (this.formGroup) {
                    this.formGroup.reset();
                }
            }
        } else {
            if (this.contactInfoPageParams.contact && !this.contactInfoPageParams.addToContactList) {
                this.contactInfoPageParams.contact = null;
                this.goContact();
            }
        }
    }
}

export interface IContactInfoPageParams {
    method:string,
    addToContactList?:boolean,
    contact?:IMyListContact,
    addActivity?:boolean
}