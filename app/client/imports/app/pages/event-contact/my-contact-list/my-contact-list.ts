import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, ViewController, AlertController, ModalController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../both/Constants";
import {ContactInfoPage, IContactInfoPageParams} from "../contact-info/contact-info";
import {IMyListContact} from "../../../../../../both/models/my-list-contact.model";
import {GroundMyListContactsCollection} from "../../../../../../both/collections/my-list-contacts";
import {GroundEventContactsCollection} from "../../../../../../both/collections/event-contacts.collection";
import {IEventContact} from "../../../../../../both/models/event-contact.model";
import {ClientMethods} from "../../../utils/ClientMethods";
@Component({
    selector: "page-my-contact-list",
    templateUrl: "my-contact-list.html"
})

export class MyContactListPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    // public program:IProgramInfo;
    public myListContacts:Array<IMyListContact>;
    public pickFromList:boolean;
    private eventContacts:Array<IEventContact>;
    private isRemovingContact:boolean = false;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.pickFromList = this.params.data.pickFromList;
        this.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
            // this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
            var programId:string = Session.get(Constants.SESSION.PROGRAM_ID);

            if (this.user) {
                // this.subscribe(Constants.PUBLICATIONS.MY_LIST_CONTACTS, () => {
                //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_LIST_CONTACTS + " <><><>");
                this.autorun(() => this.zone.run(() => {
                    this.myListContacts = GroundMyListContactsCollection.find({
                        userId: this.user._id
                    }, {
                        sort: {
                            "name.family": 1,
                            "name.given": 1
                        }
                    }).fetch();
                    //console.log("myListContacts: ", this.myListContacts);
                    this.updateContactStatus();

                    ClientMethods.checkIfEarnedAward(Constants.AWARD_KEYS.CONTACT_LIST, this.modalCtrl);
                }));
                // });

                if (this.pickFromList) {
                    // this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS, {
                    //     programId: programId,
                    //     participantId: this.user._id
                    // }, () => {
                    //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        this.eventContacts = GroundEventContactsCollection.find({
                            userId: this.user._id,
                            programId: programId
                        }).fetch();
                        //console.log("EventContacts: ", this.eventContacts);
                        this.updateContactStatus();
                    }));
                    // });
                }
            }
        }));
    }

    private updateContactStatus():void {
        if (this.myListContacts && this.eventContacts) {
            this.myListContacts.forEach((contact:IMyListContact) => {
                var eventContact:IEventContact = this.eventContacts.find(
                    (eventContact:IEventContact) => eventContact.contactId === contact._id);
                if (eventContact) {
                    contact["contacted"] = true;
                }
            });
        }
    }

    private editContact(contact?:IMyListContact):void {
        var contactInfoPageParams:IContactInfoPageParams = this.params.data.contactInfoPageParams;
        if (contactInfoPageParams) {
            contactInfoPageParams.contact = contact;
        } else {
            contactInfoPageParams = {
                method: Constants.EVENT_CONTACT_METHOD.CALL,
                addToContactList: !this.pickFromList,
                contact: contact
            };
        }

        if (!this.pickFromList) {
            if (this.isRemovingContact) {
                this.isRemovingContact = false;
            } else {
                this.nav.push(ContactInfoPage, contactInfoPageParams);
            }
        } else {
            // Remove this view and pass the selected contact back to the previous page
            var viewCtrls:Array<ViewController> = this.nav.getViews();
            var pages:Array<{page:any, params?:any}> = [];
            viewCtrls.forEach((viewCtrl:ViewController, index:number) => {
                if (viewCtrl.component !== MyContactListPage) {
                    let page:{page:any, params?:any} = {
                        page: viewCtrl.component
                    };
                    if (index === viewCtrls.length - 2) {
                        page.params = contactInfoPageParams;
                    }
                    pages.push(page);
                }
            });
            this.nav.setPages(pages);
        }
    }

    private confirmRemoveContact(contact:IMyListContact):void {
        var self = this;
        this.isRemovingContact = true;
        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-my-contact-list.alerts.promptRemoveContact.title"),
            subTitle: contact.name.display,
            message: self.translate.instant("page-my-contact-list.alerts.promptRemoveContact.message"),
            buttons: [{
                text: self.translate.instant("general.cancel"),
                role: 'cancel'
            }, {
                text: self.translate.instant("general.remove"),
                handler: () => {
                    self.removeContact(contact);
                }
            }]
        });
        alert.present();
    }

    private removeContact(contact:IMyListContact):void {
        var self = this;
        contact.hidden = true;

        ClientMethods.saveContactInfo(contact, (error, result) => {
            if (error) {
                console.error("saveContactInfo() Error: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-contact-info.errors.saveContactInfo"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok")
                    }]
                });
                alert.present();
            }
        });
        // Meteor.call("saveContactInfo", contact, (error, result) => {
        //     if (error) {
        //         console.log("saveContactInfo() Error: " + JSON.stringify(error));
        //         var errorMsg = error.reason;
        //         if (!errorMsg && error.message) {
        //             errorMsg = error.message;
        //         } else {
        //             errorMsg = error;
        //         }
        //         let alert = self.alertCtrl.create({
        //             title: self.translate.instant("page-contact-info.errors.saveContactInfo"),
        //             message: errorMsg,
        //             buttons: [{
        //                 text: self.translate.instant("general.cancel"),
        //                 role: 'cancel'
        //             }, {
        //                 text: self.translate.instant("general.tryAgain"),
        //                 handler: () => {
        //                     self.removeContact(contact);
        //                 }
        //             }]
        //         });
        //         alert.present();
        //     }
        // });
    }
}