import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, NavParams, AlertController, ModalController} from 'ionic-angular/es2015';
import {SMS} from "@ionic-native/sms";
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../../both/Constants";
import {ProgramsCollection} from "../../../../../../both/collections/programs.collection";
import {IProgramInfo} from "../../../../../../both/models/program-info.model";
import {IEventContact} from "../../../../../../both/models/event-contact.model";
import {ToastMessenger} from "../../../utils/ToastMessenger";
import {PhoneFormatter} from "../../../utils/PhoneFormatter";
import {ClientMethods} from "../../../utils/ClientMethods";

declare var device; // cordova-plugin-device

@Component({
    selector: "page-order-commitment",
    templateUrl: "order-commitment.html"
})
export class OrderCommitmentPage extends MeteorComponent implements OnInit {
    public user:Meteor.User;
    public program:IProgramInfo;
    public eventContact:IEventContact;
    public quantity:number;
    public contactNameTranslateParam:any;
    private orderConfirmationTextMessage:string;
    private maxQuantity:number = 50;
    private resendOrderConfirmation:boolean = false;

    constructor(public nav:NavController,
                public params:NavParams,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private sms:SMS) {
        super();
    }

    ngOnInit():void {
        this.eventContact = this.params.data;
        this.contactNameTranslateParam = {contactName: this.eventContact.contact.name.display};
        this.quantity = Number(this.eventContact.commitment || 0);
        this.autorun(() => this.zone.run(() => {
            this.program = ProgramsCollection.findOne({_id: Session.get(Constants.SESSION.PROGRAM_ID)});
            this.user = Meteor.user();
        }));
    }

    private onChangeQuantity(direction:string):void {
        this.zone.run(() => {
            switch (direction) {
                case '-':
                    if (this.quantity > 0) {
                        this.quantity--;
                    }
                    break;
                case '+':
                    if (this.quantity < this.maxQuantity) {
                        this.quantity++;
                    }
                    break;
            }
        });
    }

    private onSubmit(resend:boolean):void {
        let self = this;
        self.resendOrderConfirmation = resend;
        self.eventContact.commitment = self.quantity;
        self.eventContact.price = Number(self.program.contract.product.price);
        self.eventContact.receivedCommitment = true;
        if (self.eventContact.receivedCommitment) {
            if (self.eventContact.declinedCommitment) {
                self.eventContact.declinedCommitment = false;
            }
        }

        self.eventContact.eventPoints.commitment = self.eventContact.commitment * self.program.eventPoints.commitment;
        self.eventContact.status = Constants.EVENT_CONTACT_STATUS.RECEIVED_COMMITMENT;

        if (!self.eventContact._id) {
            self.logEventContact();
        } else {
            console.log("sentOrderConfirmation: " + self.eventContact.sentOrderConfirmation);
            if (self.resendOrderConfirmation || !self.eventContact.sentOrderConfirmation) {
                self.sendOrderConfirmationIfQtyNotZero();
            } else {
                self.logEventContact();
            }
        }
    }

    private sendOrderConfirmationIfQtyNotZero():void {
        let self = this;
        if (self.eventContact.commitment > 0) {
            self.promptSendOrderConfirmation();
        } else {
            self.eventContact.receivedCommitment = false;
            let alert = self.alertCtrl.create({
                title: self.translate.instant("page-order-commitment.alerts.promptSendOrderConfirmation.title"),
                message: self.translate.instant("page-order-commitment.alerts.promptSendOrderConfirmation.quantityZero"),
                buttons: [{
                    text: self.translate.instant("general.ok"),
                    handler: () => {
                        self.logEventContact();
                    }
                }],
                enableBackdropDismiss: false
            });
            alert.present();
        }
    }

    private declineCommitment():void {
        let self = this;
        self.eventContact.commitment = 0;
        if (self.eventContact.receivedCommitment) {
            self.eventContact.receivedCommitment = false;
        }
        if (self.eventContact.eventPoints.commitment) {
            self.eventContact.eventPoints.commitment = 0;
        }
        self.eventContact.status = Constants.EVENT_CONTACT_STATUS.DECLINED;
        self.eventContact.declinedCommitment = true;
        self.logEventContact();
    }

    private logEventContact():void {
        let self = this;
        //Session.set(Constants.SESSION.LOADING, true);
        ClientMethods.logEventContact(self.eventContact, (error, result) => {
            if (error) {
                console.log("logEventContact() Error: " + JSON.stringify(error));
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-contact.errors.logEventContact"),
                    message: error.reason || error.message || error,
                    buttons: [{
                        text: self.translate.instant("general.ok"),
                        role: 'cancel'
                    }]
                });
                alert.present();
            } else {
                console.log("logEventContact() result: " + JSON.stringify(result));

                // If event contact was inserted, we just saved to get contactId for order confirmation
                if (result.insertedId) {
                    self.eventContact._id = result.insertedId;
                    self.sendOrderConfirmationIfQtyNotZero();
                } else {
                    new ToastMessenger().toast({
                        type: "success",
                        message: self.translate.instant("page-order-commitment.toast.success")
                    });
                    if (!self.resendOrderConfirmation) {
                        Session.set(Constants.SESSION.CONTACT_COMPLETE, true);
                        self.zone.run(() => {
                            self.nav.pop();
                        });
                    }
                }

                ClientMethods.checkIfEarnedAward(Constants.AWARD_KEYS.COMMUNICATION, this.modalCtrl);
            }
        });

        // Meteor.call("logEventContact", self.eventContact, (error, result) => {
        //     Session.set(Constants.SESSION.LOADING, false);
        //     if (error) {
        //         console.log("logEventContact() Error: " + JSON.stringify(error));
        //         var errorMsg = error.reason;
        //         if (!errorMsg && error.message) {
        //             errorMsg = error.message;
        //         } else {
        //             errorMsg = error;
        //         }
        //         let alert = self.alertCtrl.create({
        //             title: self.translate.instant("page-contact.errors.logEventContact"),
        //             message: errorMsg,
        //             buttons: [{
        //                 text: self.translate.instant("general.ok"),
        //                 role: 'cancel'
        //             }]
        //         });
        //         alert.present();
        //     } else {
        //         console.log("logEventContact() result: " + JSON.stringify(result));
        //
        //         // If event contact was inserted, we just saved to get contactId for order confirmation
        //         if (result.insertedId) {
        //             self.eventContact._id = result.insertedId;
        //             self.sendOrderConfirmationIfQtyNotZero();
        //         } else {
        //             new ToastMessenger().toast({
        //                 type: "success",
        //                 message: self.translate.instant("page-order-commitment.toast.success")
        //             });
        //             if (!self.resendOrderConfirmation) {
        //                 Session.set(Constants.SESSION.CONTACT_COMPLETE, true);
        //                 self.nav.pop();
        //             }
        //         }
        //     }
        // });
    }

    /**
     * SMS Methods
     * */
    private promptSendOrderConfirmation():void {
        this.shortenPaymentLink();
    }

    private shortenPaymentLink():void {
        let self = this;
        let paymentLink:string = ClientMethods.getPaymentLink({
            user: this.user,
            program: this.program,
            eventContact: self.eventContact,
            appendQty: true
        });

        if (!Meteor.status().connected) {
            self.displayOrderConfirmationPrompt(paymentLink);
        } else {
            Session.set(Constants.SESSION.LOADING, true);
            Meteor.call("shortenUrl", paymentLink, (error, result) => {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    console.error("Shorten Payment Link Error: ", error);
                } else {
                    if (result.data && result.data.shortLink) {
                        paymentLink = result.data.shortLink
                    }
                }
                self.displayOrderConfirmationPrompt(paymentLink);
            });
        }
    }

    private displayOrderConfirmationPrompt(paymentLink:string):void {
        let self = this;
        let script:string = self.program.scripts.orderConfirmation;

        script = script.replace("{{programName}}", self.program.name);

        script = script.replace("{{productName}}", self.program.contract.product.label || "_____");

        script = script.replace("{{quantity}}", self.eventContact.commitment.toString());

        let totalCost:number = self.eventContact.commitment * self.program.contract.product.price;
        script = script.replace("{{totalCost}}", totalCost.toString());

        script = script.replace("{{contactName}}", self.eventContact.contact.name.given);

        let formattedPhoneNumber:string = PhoneFormatter.transform(self.user.username);
        script = script.replace("{{myPhoneNumber}}", formattedPhoneNumber);

        script = script.replace("{{me}}", self.user.profile.name.display);

        script = script.replace("{{paymentLink}}", paymentLink);

        self.orderConfirmationTextMessage = script;

        let alert = self.alertCtrl.create({
            title: self.translate.instant("page-order-commitment.alerts.promptSendOrderConfirmation.title"),
            message: self.orderConfirmationTextMessage,
            buttons: [{
                text: self.translate.instant("general.no"),
                role: "cancel",
                handler: () => {
                    self.logEventContact();
                }
            }, {
                text: self.translate.instant("page-order-commitment.alerts.promptSendOrderConfirmation.buttons.send"),
                handler: () => {
                    self.startSendingSMS();
                }
            }],
            enableBackdropDismiss: false
        });
        alert.present();
    }

    private startSendingSMS():void {
        let self = this;
        if (Meteor.isCordova) {
            if (device.platform === Constants.DEVICE.ANDROID) {
                self.sms.hasPermission().then(
                    self.smsHasPermissionSuccess.bind(self),
                    self.smsHasPermissionError.bind(self)
                );
            } else if (device.platform === Constants.DEVICE.IOS) {
                self.sendSMS()
            }
        } else {
            new ToastMessenger().toast({
                type: "error",
                message: "Can't send SMS from browser"
            });
            self.logEventContact();
        }
    }

    private smsHasPermissionSuccess():void {
        console.log("smsHasPermissionSuccess()");
        this.sendSMS();
    }

    private smsHasPermissionError(error:any):void {
        let self = this;
        console.log("smsHasPermissionError(): " + JSON.stringify(error));
        let alert = self.alertCtrl.create({
            title: self.translate.instant("general.permissionDenied"),
            message: self.translate.instant("page-contact.alerts.smsPermissionDenied.message"),
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        });
        alert.present();
    }

    private sendSMS():void {
        let self = this;
        Session.set(Constants.SESSION.LOADING, true);
        self.sms.send(self.eventContact.contact.phone, self.orderConfirmationTextMessage, {replaceLineBreaks: true}).then(
            self.smsSuccess.bind(self),
            self.smsError.bind(self)
        );
    }

    private smsSuccess():void {
        let self = this;
        console.log("Successfully sent SMS message.");
        Session.set(Constants.SESSION.LOADING, false);
        self.eventContact.status = Constants.EVENT_CONTACT_STATUS.SENT_ORDER_CONFIRMATION;
        self.eventContact.sentOrderConfirmation = true;
        self.logEventContact();
    }

    private smsError(error):void {
        let self = this;
        console.log("SMS Error: " + JSON.stringify(error));
        Session.set(Constants.SESSION.LOADING, false);
        let alertOptions:any = {
            title: self.translate.instant("page-contact.errors.sms"),
            message: JSON.stringify(error),
            buttons: [{
                text: self.translate.instant("general.ok")
            }]
        };

        if (error === Constants.METEOR_ERRORS.PERMISSION_DENIED) {
            alertOptions.title = self.translate.instant("general.permissionDenied");
            alertOptions.message = self.translate.instant("page-contact.alerts.smsPermissionDenied.message");
        }

        if (error !== "Message cancelled.") {
            let alert = self.alertCtrl.create(alertOptions);
            alert.present();
        }
    }

    /*End SMS Methods*/
}