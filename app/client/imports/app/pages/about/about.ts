import {Component, OnInit, NgZone} from '@angular/core';
import {NavController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";

@Component({
    selector: "page-about",
    templateUrl: "about.html"
})
export class AboutPage extends MeteorComponent implements OnInit {
    public appLogo:string = Constants.APP_LOGO;
    public isConnected:boolean = false;
    public version:string;

    constructor(public nav:NavController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.version = Meteor.settings.public["version"];
        this.autorun(() => this.zone.run(() => {
            console.log("Meteor server connected: " + Meteor.status().connected);
            this.isConnected = Meteor.status().connected;
        }));
    }
}