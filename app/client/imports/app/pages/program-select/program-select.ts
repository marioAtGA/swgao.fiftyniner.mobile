import {Component, NgZone, OnInit} from "@angular/core";
import {NavController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {ProgramHelper} from "../../utils/ProgramHelper";
import {Constants} from "../../../../../both/Constants";
import {IProgramInfo} from "../../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../../both/collections/programs.collection";
import {HomePage} from "../home/home";

declare var Session;

@Component({
    selector: "page-program-select",
    templateUrl: "program-select.html"
})

export class ProgramSelectPage extends MeteorComponent implements OnInit {
    public user: Meteor.User;
    public programs: Array<IProgramInfo>;
    public placeholderImageUri: string = Constants.APP_LOGO;

    constructor(public nav: NavController,
                public zone: NgZone,
                public translate: TranslateService) {
        super();
    }

    ngOnInit(): void {
        Meteor.defer(() => {
            this.autorun(() => {
                this.user = Meteor.user();
                if (this.user) {
                    this.autorun(() => this.zone.run(() => {
                        let membershipProgramIds: Array<string> = ProgramHelper.getProgramIdsForRole(
                            this.user,
                            [Constants.ROLES.MEMBER]
                        );
                        console.log("membershipProgramIds: ", membershipProgramIds);
                        this.programs = ProgramsCollection.find({
                            _id: {
                                $in: membershipProgramIds
                            }
                        }, {
                            sort: {
                                eventDate: -1
                            }
                        }).fetch();
                        console.log("membershipPrograms: ", this.programs);
                    }));
                }
            });
        });
    }

    private selectProgram(program: IProgramInfo): void {
        Session.setAuth(Constants.SESSION.PROGRAM_ID, program._id);
        this.zone.run(() => {
            this.nav.setRoot(HomePage);
        });
    }
}