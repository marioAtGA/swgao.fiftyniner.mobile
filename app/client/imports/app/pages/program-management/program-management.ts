import {Component, OnInit} from '@angular/core';
import {NavController} from 'ionic-angular/es2015';
import {MeteorComponent} from 'angular2-meteor';
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {ProgramsCollection} from "../../../../../both/collections/programs.collection";
import {IProgramInfo} from "../../../../../both/models/program-info.model";

@Component({
    selector: "page-program-management",
    templateUrl: "program-management.html"
})
export class ProgramManagementPage extends MeteorComponent implements OnInit {
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public user:Meteor.User;
    public programs:Array<IProgramInfo>;

    constructor(public nav:NavController,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.autorun(() => {
            this.user = Meteor.user();
        });

        this.autorun(() => {
            // this.subscribe(Constants.PUBLICATIONS.MANAGED_PROGRAMS, () => {
            //     console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MANAGED_PROGRAMS + " <><><>");
            //     this.programs = ProgramsCollection.find({}).fetch();
            //     console.log("programs.length: " + this.programs.length);
            // });
        });
    }
}