import {Component, OnInit, NgZone} from '@angular/core';
import {NavController, Alert} from 'ionic-angular/es2015';
import {FormBuilder, Validators, AbstractControl, FormGroup} from '@angular/forms';
import {MeteorComponent} from 'angular2-meteor';
import {Constants} from "../../../../../../../both/Constants";
import {FormValidator} from "../../../../utils/FormValidator";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {TranslateService} from "@ngx-translate/core";

@Component({
    selector: "create-account-card",
    templateUrl: "create-account-card.html"
})
export class CreateAccountCardComponent extends MeteorComponent implements OnInit {
    public createAccountForm:FormGroup;
    public formControl:{
        givenName:AbstractControl,
        familyName:AbstractControl,
        mobilePhone:AbstractControl,
        password:AbstractControl,
        confirmPassword:AbstractControl
    };

    public user:{
        mobilePhone:string,
        password:string,
        profile:{
            name:{
                given:string,
                family:string,
                display:string
            }
        }
    };
    //private trackerAutorun:any;


    constructor(public nav:NavController,
                public zone:NgZone,
                public fb:FormBuilder, public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        /* Setup form controls */
        this.createAccountForm = this.fb.group({
            'mobilePhone': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.notRegistered,
                Validators.minLength(7),
                Validators.maxLength(11),
                FormValidator.validPhoneLength
            ])],
            'givenName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'familyName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'password': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])],
            'confirmPassword': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required
            ])]
        }, {
            validator: Validators.compose([
                FormValidator.matchingFields('mismatchedPasswords', 'password', 'confirmPassword')
            ])
        });

        this.formControl = {
            givenName: this.createAccountForm.controls['givenName'],
            familyName: this.createAccountForm.controls['familyName'],
            mobilePhone: this.createAccountForm.controls['mobilePhone'],
            password: this.createAccountForm.controls['password'],
            confirmPassword: this.createAccountForm.controls['confirmPassword']
        };
        /* End for control */

        this.user = {
            mobilePhone: Constants.EMPTY_STRING,
            password: Constants.EMPTY_STRING,
            profile: {
                name: {
                    given: Constants.EMPTY_STRING,
                    family: Constants.EMPTY_STRING,
                    display: Constants.EMPTY_STRING
                }
            }
        };

        Meteor.defer(() => {
            this.autorun(() => this.zone.run(() => {
                this.user.mobilePhone = Session.get(Constants.SESSION.MOBILE_PHONE) || null;
                Session.get(Constants.SESSION.NOT_REGISTERED_ERROR);
            }), true);
        });
    }

    public removeNonDigits():void {
        var regex = /^\d*$/;
        var value:string = this.user.mobilePhone;
        if (value && !regex.test(value)) {
            var result = value.replace(/[^0-9]/g, Constants.EMPTY_STRING);
            this.user.mobilePhone = result;
        }
    }

    public createAccount():void {
        if (!Meteor.status().connected) {
            new ToastMessenger().toast({
                type: "error",
                message: self.translate.instant("general.noServerConnection"),
                title: self.translate.instant("create-account-card.errors.createAccount")
            });
            return;
        }
        if (this.createAccountForm.valid) {
            var self = this;
            Session.set(Constants.SESSION.MOBILE_PHONE, self.user.mobilePhone);
            self.user.profile.name.given = self.user.profile.name.given.trim();
            self.user.profile.name.family = self.user.profile.name.family.trim();
            Accounts.createUser({
                username: self.user.mobilePhone,
                password: self.user.password,
                profile: {
                    name: {
                        display: self.user.profile.name.given + " " + self.user.profile.name.family,
                        given: self.user.profile.name.given,
                        family: self.user.profile.name.family
                    }
                }
            }, function (error) {
                if (error) {
                    console.log("Error creating user: " + JSON.stringify(error));
                    var toastMessage = Constants.EMPTY_STRING;
                    if (error.reason) {
                        if (error.reason === Constants.METEOR_ERRORS.USERNAME_EXISTS) {
                            Session.set(Constants.SESSION.NOT_REGISTERED_ERROR, true);
                            self.formControl.mobilePhone.updateValueAndValidity({onlySelf: true});
                            error.reason = self.translate.instant(
                                "create-account-card.errors.phoneAlreadyRegistered");
                        }
                        toastMessage = error.reason;
                    } else {
                        toastMessage = error.message;
                    }
                    new ToastMessenger().toast({
                        type: "error",
                        message: toastMessage,
                        title: self.translate.instant(
                            "create-account-card.errors.createAccount")
                    });
                } else {
                    console.log("Successfully created a new user account.");
                }
            });
        }
    }

    public showSignInCard() {
        Session.set(Constants.SESSION.CREATE_ACCOUNT, !Session.get(Constants.SESSION.CREATE_ACCOUNT));
        Session.set(Constants.SESSION.MOBILE_PHONE, this.user.mobilePhone);
    }
}