import {Component, NgZone} from "@angular/core";
import {NavController} from 'ionic-angular/es2015';
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {MeteorComponent} from "angular2-meteor";
import {Constants} from "../../../../../../../both/Constants";
import {FormValidator} from "../../../../utils/FormValidator";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {TranslateService} from "@ngx-translate/core";
@Component({
    selector: "forgot-password-card",
    templateUrl: "forgot-password-card.html"
})
export class ForgotPasswordCardComponent extends MeteorComponent {
    public forgotPwForm:FormGroup;
    public formControl:{
        mobilePhone:AbstractControl
    };
    public mobilePhone:string;
    public sentSMS:boolean = false;
    public isOptIn:boolean = false;

    constructor(public nav:NavController,
                public zone:NgZone,
                public fb:FormBuilder,
                public translate:TranslateService) {
        super();
    }

    ngOnInit() {
        this.forgotPwForm = this.fb.group({
            'mobilePhone': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.registered,
                Validators.minLength(7),
                Validators.maxLength(11),
                FormValidator.validPhoneLength
            ])]
        });

        this.formControl = {
            mobilePhone: this.forgotPwForm.controls['mobilePhone']
        };

        Meteor.defer(() => {
            this.autorun(() => this.zone.run(() => {
                Session.get(Constants.SESSION.REGISTERED_ERROR);
                this.mobilePhone = Session.get(Constants.SESSION.MOBILE_PHONE) || null;
            }));
        });
    }

    private removeNonDigits():void {
        var regex = /^\d*$/;
        var value:string = this.mobilePhone;
        if (value && !regex.test(value)) {
            var result = value.replace(/[^0-9]/g, Constants.EMPTY_STRING);
            this.mobilePhone = result;
        }
    }

    public sendPasswordResetCode():void {
        var self = this;
        if (this.forgotPwForm.valid) {
            Session.set(Constants.SESSION.MOBILE_PHONE, this.mobilePhone);
            Session.set(Constants.SESSION.LOADING, true);
            Meteor.call("sendPasswordResetSMS", {username: this.mobilePhone}, (error, result) => {
                Session.set(Constants.SESSION.LOADING, false);
                if (error) {
                    console.log("Error sending forgot password SMS: " + JSON.stringify(error));
                    if (error.reason && error.reason === Constants.METEOR_ERRORS.USER_NOT_FOUND) {
                        console.log("User not found");
                        Session.set(Constants.SESSION.REGISTERED_ERROR, true);
                        self.formControl.mobilePhone.updateValueAndValidity({onlySelf: true});
                    } else {
                        var toastMessage = error.message;
                        if (error.reason) {
                            toastMessage = error.reason;
                        }
                        new ToastMessenger().toast({
                            type: "error",
                            message: toastMessage,
                            title: self.translate.instant("forgot-password-card.errors.passwordReset")
                        });
                    }
                } else {
                    console.log("result: " + JSON.stringify(result));
                    self.zone.run(()=> {
                        self.sentSMS = true;
                    });
                }
            });
        }
    }

    public showSignInCard() {
        Session.set(Constants.SESSION.FORGOT_PASSWORD, !Session.get(Constants.SESSION.FORGOT_PASSWORD));
        Session.set(Constants.SESSION.MOBILE_PHONE, this.mobilePhone);
    }
}