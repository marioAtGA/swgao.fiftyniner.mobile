import {Component, OnInit, NgZone} from "@angular/core";
import {NavController, AlertController} from "ionic-angular/es2015";
import {MeteorComponent} from "angular2-meteor";
import {FormBuilder, Validators, AbstractControl, FormGroup} from "@angular/forms";
import {TranslateService} from "@ngx-translate/core";
import {ToastMessenger} from "../../../../utils/ToastMessenger";
import {Constants} from "../../../../../../../both/Constants";
import {FormValidator} from "../../../../utils/FormValidator";
import {ImageService} from "../../../../utils/ImageService";
@Component({
    selector: "page-edit-profile",
    templateUrl: "edit-profile.html"
})
export class EditProfilePage extends MeteorComponent implements OnInit {
    public editProfileForm:FormGroup;
    public formControl:{
        givenName:AbstractControl,
        familyName:AbstractControl,
        email:AbstractControl,
        mobilePhone:AbstractControl
    };

    public user:Meteor.User;
    public imageUri:string;
    public placeholderImageUri:string = Constants.ADD_IMAGE_PLACEHOLDER_URI;
    public tracker:any;
    public initProfilePic:boolean = true;

    constructor(public nav:NavController,
                public alertCtrl:AlertController,
                public zone:NgZone,
                public translate:TranslateService,
                public fb:FormBuilder,
                private imageService:ImageService) {
        super();
    }

    ngOnInit() {
        this.editProfileForm = this.fb.group({
            'givenName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'familyName': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
            ])],
            'email': [Constants.EMPTY_STRING, Validators.compose([
                // Validators.required,
                FormValidator.validEmail,
                FormValidator.notRegistered
            ])],
            'mobilePhone': [Constants.EMPTY_STRING, Validators.compose([
                Validators.required,
                FormValidator.notRegistered,
                Validators.minLength(7),
                Validators.maxLength(11),
                FormValidator.validPhoneLength
            ])]
        });

        this.formControl = {
            givenName: this.editProfileForm.controls['givenName'],
            familyName: this.editProfileForm.controls['familyName'],
            email: this.editProfileForm.controls['email'],
            mobilePhone: this.editProfileForm.controls['mobilePhone']
        };

        this.tracker = Tracker.autorun(() => this.zone.run(() => {
            this.user = Meteor.user();
            if (this.user) {
                if (!this.user.emails || this.user.emails.length === 0) {
                    this.user.emails = [{address: Constants.EMPTY_STRING, verified: false}];
                }

                if (this.initProfilePic) {
                    this.initProfilePic = false;
                    Session.set(Constants.SESSION.imageUri, this.user.profile.picture);
                }
            }

            this.imageUri = Session.get(Constants.SESSION.imageUri);
        }));

        this.autorun(() => {
            Session.get(Constants.SESSION.NOT_REGISTERED_ERROR);
        });
    }

    public updateProfile():void {
        var self = this;
        if (this.editProfileForm.valid) {
            self.user.profile.picture = Session.get(Constants.SESSION.imageUri);
            if (!Meteor.status().connected) {
                let alert = self.alertCtrl.create({
                    title: self.translate.instant("page-landing.alerts.noConnection.title"),
                    message: self.translate.instant("page-landing.alerts.noConnection.message"),
                    buttons: [{
                        text: self.translate.instant("general.cancel"),
                        role: "cancel"
                    }, {
                        text: self.translate.instant("general.tryAgain"),
                        handler: () => {
                            self.updateProfile();
                        }
                    }],
                    enableBackdropDismiss: false
                });
                alert.present();
            } else {
                Session.set(Constants.SESSION.LOADING, true);
                Meteor.call("updateAccountInfo", self.user, (error, result) => {
                    Session.set(Constants.SESSION.LOADING, false);
                    if (error) {
                        var toastMessage = Constants.EMPTY_STRING;
                        if (error.reason) {
                            if (error.reason === Constants.METEOR_ERRORS.EMAIL_EXISTS) {
                                Session.set(Constants.SESSION.NOT_REGISTERED_ERROR, true);
                                self.formControl.email.updateValueAndValidity({onlySelf: true});
                                error.reason = self.translate.instant(
                                    "create-account-card.errors.emailAlreadyRegistered");
                            } else if (error.reason === Constants.METEOR_ERRORS.USERNAME_EXISTS) {
                                Session.set(Constants.SESSION.NOT_REGISTERED_ERROR, true);
                                self.formControl.mobilePhone.updateValueAndValidity({onlySelf: true});
                                error.reason = self.translate.instant(
                                    "create-account-card.errors.phoneAlreadyRegistered");
                            }
                            toastMessage = error.reason;
                        } else {
                            toastMessage = error.message;
                        }
                        new ToastMessenger().toast({
                            type: "error",
                            message: toastMessage,
                            title: self.translate.instant("page-edit-profile.errors.updateProfile")
                        });
                    } else {
                        new ToastMessenger().toast({
                            type: "success",
                            message: self.translate.instant("page-edit-profile.updateProfileSuccess")
                        });
                        self.nav.pop();
                    }
                });
            }
        }
    }

    /*Life Cycle*/
    ionViewWillLeave() {
        // stop tracker so item does not continue to update
        this.tracker.stop();
    }
}