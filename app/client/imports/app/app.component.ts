import {Component, OnInit, NgZone, ViewChild} from "@angular/core";
import {MeteorComponent} from "angular2-meteor";
import {
    Platform,
    LoadingController,
    Loading,
    App,
    NavController,
    AlertController,
    Alert,
    ModalController,
    Content
} from "ionic-angular/es2015";
import {SplashScreen} from "@ionic-native/splash-screen";
import {StatusBar} from "@ionic-native/status-bar";
import {Constants} from "../../../both/Constants";
import {TranslateService} from "@ngx-translate/core";
import {DeviceReady} from "./utils/DeviceReady";
import {LandingPage} from "./pages/landing/landing";
import {LoginPage} from "./pages/account/login/login";
import {HomePage} from "./pages/home/home";
import {AboutPage} from "./pages/about/about";
import {AccountMenuPage} from "./pages/account/account-menu/account-menu";
import {ProgramsCollection} from "../../../both/collections/programs.collection";
import {ProgramManagementPage} from "./pages/program-management/program-management";
import {ProgramRegistrationPage} from "./pages/program-registration/program-registration";
import {ProgramSelectPage} from "./pages/program-select/program-select";
import {IProgramInfo} from "../../../both/models/program-info.model";
import {ProgramHelper} from "./utils/ProgramHelper";
import {MyContactListPage} from "./pages/event-contact/my-contact-list/my-contact-list";
import {GroundMyListContactsCollection} from "../../../both/collections/my-list-contacts";
import {RequestHelper} from "./utils/RequestHelper";
import {IRankings} from "../../../both/models/rankings.model";
import {RankingsCollection} from "../../../both/collections/rankings.collection";
import {IPushNotification} from "../../../both/models/push-notification.model";
import {NotificationModalPage} from "./modals/notification-modal/notification-modal";
import {AwardProgressHelper} from "./utils/AwardProgressHelper";
import {AwardProgressModalPage} from "./modals/award-progress-modal/award-progress-modal";
import {AppRate} from "@ionic-native/app-rate";
import {Device} from "@ionic-native/device";

declare var Roles;
declare var navigator;
declare var hockeyapp;

@Component({
    selector: "ion-app",
    templateUrl: "app.component.html"
})
export class AppComponent extends MeteorComponent implements OnInit {
    @ViewChild('leftMenu') leftMenu:any;
    @ViewChild('content') nav:any;
    @ViewChild(Content) content:Content;

    // Set the root (or first) page
    public rootPage:any = LandingPage;
    public appName:string;
    public appIcon:string = Constants.APP_ICON;
    public user:Meteor.User;
    public pages:Array<INavigationMenuPage>;
    public userPages:Array<INavigationMenuPage>;
    public noUserPages:Array<INavigationMenuPage>;
    private isLoading:boolean = false;
    private loading:Loading;
    private managedPrograms:Array<IProgramInfo>;
    private exitAlert:Alert;
    private isIPhoneX:boolean = false;

    constructor(public app:App,
                public platform:Platform,
                public loadingCtrl:LoadingController,
                public alertCtrl:AlertController,
                public modalCtrl:ModalController,
                public zone:NgZone,
                public translate:TranslateService,
                private splashscreen:SplashScreen,
                private statusbar:StatusBar,
                private appRate:AppRate,
                private device:Device) {
        super();
    }

    ngOnInit():void {
        this.parseUrl();
        this.initializeApp();
        // set the nav menu title to the application name from settings.json
        this.appName = Meteor.settings.public["appName"];

        // set our app's pages
        // title references a key in the language JSON to be translated by the translate pipe in the HTML
        this.noUserPages = [{
            icon: "log-in",
            title: 'page-login.signIn',
            component: LoginPage,
            rootPage: true
        }];
        this.pages = [{
            icon: "information-circle",
            title: "page-about.title",
            component: AboutPage,
            rootPage: false
        }, {
            icon: "chatbubbles",
            title: "menu.feedback",
            component: null,
            rootPage: false,
            func: () => {
                if (Meteor.isCordova) {
                    hockeyapp.feedback();
                }
            }
        }, {
            icon: "star",
            title: "menu.rateApp",
            component: null,
            rootPage: false,
            func: () => {
                if (Meteor.isCordova) {
                    this.appRate.promptForRating(true);
                }
            }
        }];

        this.translate.onLangChange.subscribe(() => {
            Session.set(Constants.SESSION.TRANSLATIONS_READY, true);
        });

        this.autorun(() => this.zone.run(() => {
            // Use this to update component variables based on reactive sources.
            // (i.e. Monogo collections or Session variables)

            // User will be null upon initialization
            this.user = Meteor.user();
            //console.log("user: " + JSON.stringify(this.user));

            if (this.user) {
                if (this.nav && this.nav.getActive()) {
                    let viewCtrl = this.nav.getActive();
                    if (viewCtrl.component === LoginPage) {
                        this.nav.setRoot(LandingPage);
                    }
                }
            }
            this.setUserPages();
        }));

        this.autorun(() => this.zone.run(() => {
            if (Session.get(Constants.SESSION.PLATFORM_READY)) {
                this.platformReady();

                // Reset PLATFORM_READY flag so styles will be applied correctly with hotcode push
                Session.set(Constants.SESSION.PLATFORM_READY, false);
            }
        }));

        // Global loading dialog
        this.autorun(() => {
            // Use Session.set(Constants.SESSION.LOADING, true) to trigger loading dialog
            if (Session.get(Constants.SESSION.LOADING)) {
                if (this.nav) {
                    // Delay to prevent showing if loaded quickly
                    Meteor.setTimeout(() => {
                        if (!this.loading && Session.get(Constants.SESSION.LOADING)) {
                            this.loading = this.loadingCtrl.create({
                                spinner: 'crescent'
                                //content: 'Logging in...'
                            });
                            this.loading.present();
                            this.isLoading = true;
                        }
                    }, 500);
                }
            } else {
                if (this.isLoading && this.loading) {
                    this.loading.dismiss();
                    this.loading = null;
                    this.isLoading = false;
                }
            }
        });

        // Monitor server connection status
        this.autorun(() => {
            var meteorStatus:any = Meteor.status();
            //console.log("Meteor server connection status: " + meteorStatus.status);
        });

        // App subscriptions
        this.autorun(() => {
            if (Meteor.user()) {
                this.subscribe(Constants.PUBLICATIONS.MANAGED_PROGRAMS, () => {
                    //console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MANAGED_PROGRAMS + " <><><>");
                    if (this.user) {
                        var programIds:Array<string> = ProgramHelper.getProgramIdsForRole(
                            this.user, [
                                Constants.ROLES.PROGRAM_MANAGEMENT,
                                Constants.ROLES.ADMIN
                            ]
                        );

                        this.managedPrograms = ProgramsCollection.find({_id: {$in: programIds}}).fetch();
                    }
                    this.setUserPages();
                });

                this.subscribe(Constants.PUBLICATIONS.MY_LIST_CONTACTS, () => {
                    //console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.MY_LIST_CONTACTS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        if (this.user) {
                            var myListContacts = GroundMyListContactsCollection.find({
                                userId: this.user._id
                            }, {
                                sort: {
                                    "name.family": 1,
                                    "name.given": 1
                                }
                            }).fetch();
                            //console.log("myListContacts: ", myListContacts);
                        }
                    }));
                });

                var membershipProgramIds:Array<string> = ProgramHelper.getProgramIdsForRole(
                    this.user,
                    [Constants.ROLES.MEMBER]
                );

                this.subscribe(Constants.PUBLICATIONS.PROGRAM_MEMBERSHIPS, membershipProgramIds, () => {
                    //console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_MEMBERSHIPS + " <><><>");
                    this.autorun(() => this.zone.run(() => {
                        //console.log("membershipProgramIds: ", membershipProgramIds);
                        let programs = ProgramsCollection.find({_id: {$in: membershipProgramIds}}).fetch();
                        //console.log("membershipPrograms: ", programs);
                    }));
                });

                var programId:string = Session.get(Constants.SESSION.PROGRAM_ID);
                // if (programId) {
                this.autorun(() => this.zone.run(() => {
                    // let program:IProgramInfo = ProgramsCollection.findOne({_id: programId});
                    // if (program) {
                    this.subscribe(Constants.PUBLICATIONS.RANKINGS, membershipProgramIds, () => {
                        //console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.RANKINGS + " <><><>");
                        var rankings:Array<IRankings> = RankingsCollection.find({}, {
                            sort: {
                                totalPoints: -1,
                                lastUpdated: 1
                            }
                        }).fetch();
                        //console.log("rankings: " + rankings.length, rankings);
                    });

                    this.subscribe(Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS, membershipProgramIds, () => {
                        // "<><><> Subscribed to: " +
                        //     console.log(   Constants.PUBLICATIONS.PROGRAM_PARTICIPANTS + " <><><>");
                    });

                    this.subscribe(Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS, {
                        programIds: membershipProgramIds,
                        participantId: this.user._id
                    }, () => {
                        // console.log("<><><> Subscribed to: " +
                        //     Constants.PUBLICATIONS.PARTICIPANT_EVENT_CONTACTS + " <><><>");
                    });

                    this.subscribe(Constants.PUBLICATIONS.PROGRAM_GROUPS, membershipProgramIds, () => {
                        // console.log("<><><> Subscribed to: " + Constants.PUBLICATIONS.PROGRAM_GROUPS + " <><><>");
                    });
                    // }
                }));
                // }
            }
        });

        // Password reset
        this.autorun(() => {
            if (Session.get(Constants.SESSION.RESET_PASSWORD_TOKEN)) {
                //console.log("Found password reset token!  Setting root page to LoginPage...");
                Session.set(Constants.SESSION.RESET_PASSWORD, true);
                this.zone.run(() => {
                    this.nav.setRoot(LoginPage);
                });
            }
        });

        // Handle push notifications
        this.autorun(() => {
            if (Session.get(Constants.SESSION.NOTIFICATION_RECEIVED)) {
                Session.set(Constants.SESSION.NOTIFICATION_RECEIVED, false);
                var notification:IPushNotification = Session.get(Constants.SESSION.PUSH_NOTIFICATION);
                switch (notification.payload.action) {
                    case Constants.PUSH_NOTIFICATION_ACTIONS.MESSAGE:
                        let modal = this.modalCtrl.create(NotificationModalPage, notification);
                        modal.present();
                        break;
                    case Constants.PUSH_NOTIFICATION_ACTIONS.AWARD:
                        if (this.user && this.user._id === notification.payload.userId) {
                            let program:IProgramInfo = ProgramsCollection.findOne({_id: notification.payload.programId});
                            var awardProgressHelper:AwardProgressHelper = new AwardProgressHelper(this.user, program);
                            let modal = this.modalCtrl.create(AwardProgressModalPage, {
                                awardInfo: awardProgressHelper.awardInfos[notification.payload.awardKey],
                                presentAward: true
                            });
                            modal.present();
                        }
                        break;
                }
            }
        });
    }

    private setUserPages():void {
        var hideHomePage:boolean = true;
        var membershipProgramIds:Array<string> = ProgramHelper.getProgramIdsForRole(
            this.user,
            [Constants.ROLES.MEMBER]
        );

        if (membershipProgramIds.length > 0 && Session.get(Constants.SESSION.PROGRAM_ID)) {
            hideHomePage = false;
        }

        this.userPages = [{
            icon: "home",
            title: 'page-home-tab.title',
            component: HomePage,
            rootPage: true,
            hide: hideHomePage
        }, {
            icon: "contacts",
            title: 'page-my-contact-list.title',
            component: MyContactListPage
        }, {
            icon: "filing",
            title: 'page-program-management.title',
            component: ProgramManagementPage,
            hide: true //(!this.managedPrograms || !(this.managedPrograms.length > 0))
        }, {
            icon: "clipboard",
            title: 'page-program-registration.title',
            component: ProgramRegistrationPage
        }, {
            icon: "swap",
            title: 'page-program-select.title',
            component: ProgramSelectPage,
            hide: !(membershipProgramIds.length > 0)
        }];
    }

    private initializeApp() {
        var self = this;
        this.platform.ready().then(() => {
            // The platform is now ready. Note: if this callback fails to fire, follow
            // the Troubleshooting guide for a number of possible solutions:
            //
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            //
            // First, let's hide the keyboard accessory bar (only works natively) since
            // that's a better default:
            //
            // Keyboard.setAccessoryBarVisible(false);
            // For example, we might change the StatusBar color. This one below is
            // good for dark backgrounds and light text:
            if (Meteor.isCordova) {
                // Keyboard.setAccessoryBarVisible(false);

                // Style status bar
                // StatusBar.styleDefault();
                this.statusbar.styleLightContent();

                // Set color of Android status bar background
                // iOS note: you must call StatusBar.overlaysWebView(false) to enable color changing.
                // StatusBar.overlaysWebView(false);

                //* Complimentary hex value for primary color in theme/app.variables.scss *//
                this.statusbar.backgroundColorByHexString("#C60817");

                this.splashscreen.hide();

                DeviceReady.onDeviceReady();

                self.platform.registerBackButtonAction(self.onBackButtonPressed.bind(self));
                let deviceModel = this.device.model;
                console.log("deviceModel: ", deviceModel);
                let isIPhone:boolean = deviceModel.includes("iPhone");
                if (isIPhone) {
                    let modelInfo = this.device.model.replace("iPhone", Constants.EMPTY_STRING);
                    console.log("modelInfo: ", modelInfo);
                    let numbers = modelInfo.split(",");
                    console.log("numbers: ", numbers);
                    let modelNumber:number = parseInt(numbers[0]);
                    console.log("modelNumber: ", modelNumber);
                    if (modelNumber >= 10) {
                        console.log("Device is iPhone X or newer");
                        this.isIPhoneX = true;
                    }
                } else if (deviceModel === "x86_64") {
                    this.isIPhoneX = true;
                }
                Session.set(Constants.SESSION.IS_IPHONE_X_LAYOUT, this.isIPhoneX);
            }

            Session.set(Constants.SESSION.PLATFORM_READY, true);
        });
    }

    private platformReady():void {
        this.initializeTranslateServiceConfig();
        this.setStyle();
    }

    private initializeTranslateServiceConfig() {
        var prefix = '/i18n/';
        var suffix = '.json';

        var userLang = navigator.language.split('-')[0];
        userLang = /(en|es)/gi.test(userLang) ? userLang : 'en';

        this.translate.setDefaultLang('en');
        let langPref = Session.get(Constants.SESSION.LANGUAGE);
        if (langPref) {
            userLang = langPref;
        }
        Session.set(Constants.SESSION.LANGUAGE, userLang);
        this.translate.use(userLang);
    }

    private setStyle():void {
        // Change value of the meta tag
        var links:any = document.getElementsByTagName("link");
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            if (link.getAttribute("rel").indexOf("style") != -1 && link.getAttribute("title")) {
                link.disabled = true;
                if (link.getAttribute("title") === this.getBodyStyle())
                    link.disabled = false;
            }
        }
    }

    private getBodyStyle():string {
        var bodyTag:any = document.getElementsByTagName("ion-app")[0];
        var bodyClass = bodyTag.className;
        var classArray = bodyClass.split(" ");
        var bodyStyle = classArray[2];
        return bodyStyle;
    }

    private openPage(page:INavigationMenuPage) {
        this.navigate({page: page.component, setRoot: page.rootPage, func: page.func});
    }

    private showAccountMenu():void {
        this.navigate({page: AccountMenuPage, setRoot: false});
    }

    private logout():void {
        Session.set(Constants.SESSION.LOADING, true);
        Meteor.logout(() => {
            Session.set(Constants.SESSION.LOADING, false);
            console.log("Logged out!!!");
            this.navigate({page: LandingPage, setRoot: true});
        });
    }

    private navigate(location:{page:any, setRoot:boolean, func?:Function}):void {
        // close the menu when clicking a link from the menu
        // getComponent selector is the component id attribute
        this.leftMenu.close().then(() => {
            if (location.page) {
                // navigate to the new page if it is not the current page
                let viewCtrl = this.nav.getActive();
                if (viewCtrl.component !== location.page) {
                    if (location.setRoot) {
                        this.nav.setRoot(location.page);
                    } else {
                        this.nav.push(location.page);
                    }
                } else if (viewCtrl.component === HomePage) {
                    this.nav.setRoot(HomePage);
                }
            } else {
                if (location.func) {
                    location.func();
                }
            }
        });
    }

    private onBackButtonPressed(event:any):void {
        var self = this;
        //console.log("onBackButtonPressed()");
        var activeNav:NavController = self.app.getActiveNav();
        if (!activeNav.canGoBack()) {
            //console.log("On Root Page, can't go back, prompt exit app.");
            if (!self.exitAlert) {
                self.exitAlert = self.alertCtrl.create({
                    title: self.translate.instant("general.alerts.exit.title"),
                    message: self.translate.instant("general.alerts.exit.message"),
                    buttons: [{
                        text: self.translate.instant("general.no"),
                        role: 'cancel',
                        handler: () => {
                        }
                    }, {
                        text: self.translate.instant("general.yes"),
                        handler: () => {
                            navigator.app.exitApp();
                        }
                    }]
                });
                self.exitAlert.present();
            } else {
                this.exitAlert.dismiss();
                this.exitAlert = null;
            }
        } else {
            activeNav.pop();
        }
    }

    private parseUrl():void {
        if (!Meteor.isCordova) {
            var path = RequestHelper.getPath(this.platform.url());
            var urlParams:any = RequestHelper.getUrlParams(this.platform.url());
            if (urlParams) {
                var scope:any = urlParams.scope;
                if (scope) {
                    urlParams.scope = scope.split(',');
                }
                urlParams.string = RequestHelper.getUrlParamString(this.platform.url());
            }
            Session.set(Constants.SESSION.PATH, path);
            Session.set(Constants.SESSION.URL_PARAMS, urlParams);
            //console.log("path: " + path);
            //console.log("urlParams: " + JSON.stringify(urlParams));
            window.history.pushState('', document.title, Meteor.absoluteUrl());
        }
    }
}

export interface INavigationMenuPage {
    icon?:string,
    title:string,
    component:any,
    rootPage?:boolean,
    navParams?:any,
    hide?:boolean,
    func?:Function
}
