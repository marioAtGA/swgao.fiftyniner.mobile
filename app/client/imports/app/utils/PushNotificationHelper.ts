import {Constants} from "../../../../both/Constants";
import {IPushNotification} from "../../../../both/models/push-notification.model";
declare var Push;
export class PushNotificationHelper {
    public static configure():void {
        Push.Configure({
            android: {
                senderID: 540876162888,
                alert: true,
                badge: true,
                sound: true,
                vibrate: true,
                icon: 'ic_status_bar',
                iconColor: '#DD0A16',
                clearNotifications: false
            },
            ios: {
                alert: true,
                badge: true,
                sound: true
            }
        });
    }

    public static setPushListeners():void {
        PushNotificationHelper.configure();
        // Internal events
        Push.addListener('token', function (token) {
            // Token is { apn: 'xxxx' } or { gcm: 'xxxx' }
            console.log("Token: " + JSON.stringify(token));
            Session.set(Constants.SESSION.PUSH_TOKEN, token);
        });

        Push.addListener('error', function (err) {
            console.log("Push Event Listener - Error: " + JSON.stringify(err));
            if (err.type == 'apn.cordova') {
                console.log(err.error);
            }
        });

        Push.addListener('register', function (evt) {
            // Platform specific event - not really used
            console.log("Push Event Listener - Register: " + evt);
        });

        Push.addListener('alert', function (notification) {
            // Called when message got a message in foreground
            console.log("Push Event Listener - alert: " + JSON.stringify(notification));
            PushNotificationHelper.handlePushNotification(notification);
        });

        Push.addListener('sound', function (notification) {
            // Called when message got a sound
            console.log("Push Event Listener - sound: " + JSON.stringify(notification));
        });

        Push.addListener('badge', function (notification) {
            // Called when message got a badge
            console.log("Push Event Listener - badge: " + JSON.stringify(notification));
        });

        Push.addListener('startup', function (notification) {
            // Called when message received on startup (cold+warm)
            console.log("Push Event Listener - startup: " + JSON.stringify(notification));
            PushNotificationHelper.handlePushNotification(notification);
        });

        Push.addListener('message', function (notification) {
            // Called when notification is received when app is in foreground
            console.log("Push EventListener - message: " + JSON.stringify(notification))
        });
    }

    private static handlePushNotification(notification:any):void {
        Session.set(Constants.SESSION.PUSH_NOTIFICATION, notification);
        Session.set(Constants.SESSION.NOTIFICATION_RECEIVED, true);
        Meteor.call("receivePushNotification", notification);
    }
}