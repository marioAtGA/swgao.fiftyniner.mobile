import {IProgramInfo} from "../../../../both/models/program-info.model";
import {AwardInfo} from "../../../../both/models/award-info.model";
import {Constants} from "../../../../both/Constants";
import {IEventContact} from "../../../../both/models/event-contact.model";
import {GroundEventContactsCollection} from "../../../../both/collections/event-contacts.collection";
import {IMyListContact} from "../../../../both/models/my-list-contact.model";
import {GroundMyListContactsCollection} from "../../../../both/collections/my-list-contacts";
export class AwardProgressHelper {
    public user:Meteor.User;
    public program:IProgramInfo;
    public awardInfos:any = {};

    constructor(user:Meteor.User, program:IProgramInfo) {
        this.user = user;
        this.program = program;

        var eventContacts:Array<IEventContact> = GroundEventContactsCollection.find({
            userId: this.user._id,
            programId: this.program._id
        }).fetch();

        var myListContacts:Array<IMyListContact> = GroundMyListContactsCollection.find({
            userId: this.user._id
        }).fetch();

        var myListContactsCount:number = 0;
        var eventContactsCount:number = 0;
        var purchased:number = 0;

        if (myListContacts) {
            myListContactsCount = myListContacts.length;
        }

        if (eventContacts) {
            eventContactsCount = eventContacts.length;
            eventContacts.forEach((eventContact:IEventContact) => {
                if (eventContact.purchased) {
                    purchased += eventContact.purchased;
                }
            });
        }

        Object.keys(Constants.AWARD_INFO).forEach((awardKey:string) => {
            let award:AwardInfo = new AwardInfo(Constants.AWARD_INFO[awardKey]);
            award.current = 0;
            award.goal = 0;

            switch (awardKey) {
                case Constants.AWARD_KEYS.REGISTRATION:
                    award.current = 1;
                    award.goal = 1;
                    award.reward = this.program.eventPoints.registration;
                    break;
                case Constants.AWARD_KEYS.CONTACT_LIST:
                    award.current = myListContactsCount;
                    award.goal = this.program.contactListGoal;
                    award.reward = this.program.eventPoints.contactListGoal;
                    break;
                case Constants.AWARD_KEYS.COMMUNICATION:
                    award.current = eventContactsCount;
                    award.goal = this.program.contactListGoal;
                    award.reward = this.program.eventPoints.contactGoal;
                    break;
                case Constants.AWARD_KEYS.SALES:
                    award.current = purchased;
                    award.goal = this.program.salesGoal;
                    award.reward = this.program.eventPoints.salesGoalAward;
                    break;
            }

            var progress:number = (award.current / award.goal);
            if (progress >= 1) {
                progress = 1;
            }
            award.progress = progress;

            this.awardInfos[awardKey] = award;
        });
    }
}