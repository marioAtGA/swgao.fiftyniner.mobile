import {IMyListContact, MyListContact} from "../../../../both/models/my-list-contact.model";
import {GroundEventContactsCollection} from "../../../../both/collections/event-contacts.collection";
import {GroundMyListContactsCollection} from "../../../../both/collections/my-list-contacts";
import {IEventContact, EventContact} from "../../../../both/models/event-contact.model";
import * as moment from "moment";
import {IProgramInfo} from "../../../../both/models/program-info.model";
import {ProgramsCollection} from "../../../../both/collections/programs.collection";
import {ModalController} from "ionic-angular/es2015";
import {Constants} from "../../../../both/Constants";
import {AwardProgressHelper} from "./AwardProgressHelper";
import {AwardProgressModalPage} from "../modals/award-progress-modal/award-progress-modal";

export class ClientMethods {
    public static saveContactInfo(contact:IMyListContact, callback:Function):void {
        contact = new MyListContact(contact);
        let contactId:string = contact._id;
        console.log("contactId: " + contactId);
        delete contact._id;
        let hiddenWithNoEventContacts:boolean = false;
        if (contactId && contact.hidden) {
            let communications:number = GroundEventContactsCollection.find({contactId: contactId}).count();
            hiddenWithNoEventContacts = (communications === 0);
        }
        if (hiddenWithNoEventContacts) {
            console.log("Removing contact...");
            try {
                let result = GroundMyListContactsCollection.remove({_id: contactId});
                callback(false, result);
            } catch (error) {
                callback(error);
            }
        } else {
            console.log("Saving contact info...");
            if (!contactId) {
                console.log("Inserting new contact...");
                try {
                    let insertedId = GroundMyListContactsCollection.insert(contact);
                    callback(false, {insertedId: insertedId});
                } catch (error) {
                    callback(error);
                }
            } else {
                console.log("Updating existing contact...");
                try {
                    let result = GroundMyListContactsCollection.update({_id: contactId}, {$set: contact});
                    callback(false, result);
                } catch (error) {
                    callback(error);
                }
            }
        }
    }

    public static logEventContact(eventContact:IEventContact, callback:Function):void {
        eventContact.updated = moment().toISOString();
        eventContact = new EventContact(eventContact);
        delete eventContact.contact;

        var query:any = {
            userId: eventContact.userId,
            programId: eventContact.programId,
            contactId: eventContact.contactId
        };
        console.log("Event contact query: " + JSON.stringify(query));

        var existingEventContact:IEventContact = GroundEventContactsCollection.findOne(query);
        console.log("existingEventContact: ", existingEventContact);

        if (existingEventContact) {
            console.log("Updating existing event contact...");
            try {
                delete eventContact._id;
                let result = GroundEventContactsCollection.update({_id: existingEventContact._id},
                    {$set: eventContact});
                callback(false, result);
            } catch (error) {
                console.error("logEventContact() Error: " + JSON.stringify(error));
                callback(error);
            }
        } else {
            console.log("Inserting new event contact...");
            try {
                let insertedId = GroundEventContactsCollection.insert(eventContact);
                callback(false, {insertedId: insertedId});
            } catch (error) {
                console.error("logEventContact() Error: " + JSON.stringify(error));
                callback(error);
            }
        }
    }

    public static checkIfEarnedAward(awardKey:string, modalCtrl:ModalController):void {
        var user:Meteor.User = Meteor.user();
        var programId = Session.get(Constants.SESSION.PROGRAM_ID);
        let program:IProgramInfo = ProgramsCollection.findOne({
            _id: programId
        });

        var presentAward:boolean = false;

        if (user && program) {
            switch (awardKey) {
                case Constants.AWARD_KEYS.CONTACT_LIST:
                    var myListContacts:Array<IMyListContact> = GroundMyListContactsCollection.find({
                        userId: user._id
                    }).fetch();
                    if (myListContacts.length >= program.contactListGoal) {
                        if (!user.profile.programs || !user.profile.programs[programId] || !user.profile.programs[programId].eventPoints || (!user.profile.programs[programId].eventPoints.contactListGoal && user.profile.programs[programId].eventPoints.contactListGoal !== 0)) {
                            let modifier:any = {$set: {}};
                            let modifierKeyString:string = "profile.programs." + programId + ".eventPoints.contactListGoal";
                            modifier.$set[modifierKeyString] = program.eventPoints.contactListGoal;
                            modifierKeyString = "profile.programs." + programId + ".eventPoints.updated";
                            modifier.$set[modifierKeyString] = moment().toISOString();
                            Meteor.users.update({_id: user._id}, modifier);
                            presentAward = true;
                        }
                    } else {
                        if (user.profile.programs && user.profile.programs[programId] && user.profile.programs[programId].eventPoints && user.profile.programs[programId].eventPoints.contactListGoal) {
                            let modifier:any = {$set: {}};
                            let modifierKeyString:string = "profile.programs." + programId + ".eventPoints.contactListGoal";
                            modifier.$set[modifierKeyString] = Constants.EMPTY_STRING;
                            modifierKeyString = "profile.programs." + programId + ".eventPoints.updated";
                            modifier.$set[modifierKeyString] = moment().toISOString();
                            Meteor.users.update({_id: user._id}, modifier);
                        }
                    }
                    break;
                case Constants.AWARD_KEYS.COMMUNICATION:
                    let eventContacts:Array<IEventContact> = GroundEventContactsCollection.find({
                        userId: user._id,
                        programId: program._id,
                        hidden: {$in: [null, false]}
                    }).fetch();
                    if (eventContacts.length >= program.contactListGoal) {
                        console.log("Award points for contact goal");
                        if (!user.profile.programs || !user.profile.programs[program._id] || !user.profile.programs[program._id].eventPoints || (!user.profile.programs[program._id].eventPoints.contactGoal && user.profile.programs[program._id].eventPoints.contactGoal !== 0)) {
                            let modifier:any = {$set: {}};
                            let modifierKeyString:string = "profile.programs." + program._id + ".eventPoints.contactGoal";
                            modifier.$set[modifierKeyString] = program.eventPoints.contactGoal;
                            modifierKeyString = "profile.programs." + program._id + ".eventPoints.updated";
                            modifier.$set[modifierKeyString] = moment().toISOString();
                            Meteor.users.update({_id: user._id}, modifier);
                            presentAward = true;
                        }
                    }
                    break;
            }
        }

        if (presentAward && modalCtrl) {
            var awardProgressHelper:AwardProgressHelper = new AwardProgressHelper(user, program);
            let modal = modalCtrl.create(AwardProgressModalPage, {
                awardInfo: awardProgressHelper.awardInfos[awardKey],
                presentAward: true
            });
            modal.present();
        }
    }

    public static getPaymentLink(data:{
        user: Meteor.User,
        program:IProgramInfo,
        eventContact:IEventContact,
        appendQty?: boolean
    }):string {
        let paymentLink: string = data.program.contract.shopUrl;
        let isQsp: boolean = data.program.contract.divisionCode === Constants.DIVISION_CODE.QSPCA;
        let isOnlineContract: boolean = isQsp || !!data.program.contract.contractId;
        if (isOnlineContract) {
            paymentLink = paymentLink + Constants.PAYMENT_LINK_PARAMS.ONLINE_ID;
        }
        paymentLink = paymentLink + data.user.profile["onlineIds"][data.program.contract.contractNumber];
        paymentLink = paymentLink + (isOnlineContract ? "&" : "?");
        paymentLink = paymentLink + Constants.PAYMENT_LINK_PARAMS.GTID + data.eventContact._id;
        if (data.appendQty) {
            paymentLink += "&qty=" + data.eventContact.commitment;
        }

        return paymentLink;
    }
}