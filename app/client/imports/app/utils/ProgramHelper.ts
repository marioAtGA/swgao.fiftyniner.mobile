declare var Roles;

export class ProgramHelper {

    public static getProgramIdsForRole(user:Meteor.User, roles:Array<string>):Array<string> {
        var roleProgramIds:Array<string> = [];
        if (user && user["roles"]) {
            var programIds:Array<string> = Object.keys(user["roles"]) || [];
            programIds.forEach((programId:string) => {
                if (Roles.userIsInRole(user._id, roles, programId)) {
                    roleProgramIds.push(programId);
                }
            });
        }
        return roleProgramIds;
    }
}