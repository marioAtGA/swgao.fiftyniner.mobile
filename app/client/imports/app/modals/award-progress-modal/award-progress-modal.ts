import {Component, OnInit, NgZone, ViewChild, ElementRef} from "@angular/core";
import {NavController, NavParams, ViewController} from 'ionic-angular/es2015';
import {MeteorComponent} from "angular2-meteor";
import {TranslateService} from "@ngx-translate/core";
import {Constants} from "../../../../../both/Constants";
import {IProgramInfo} from "../../../../../both/models/program-info.model";
import {IAwardInfo} from "../../../../../both/models/award-info.model";
declare var ProgressBar;
@Component({
    selector: "page-award-progress-modal",
    templateUrl: "award-progress-modal.html"
})
export class AwardProgressModalPage extends MeteorComponent implements OnInit {
    @ViewChild('awardProgressElement') awardProgressElement:ElementRef;
    public user:Meteor.User;
    public program:IProgramInfo;
    public awardInfo:IAwardInfo;
    public translateParam:{goal:number} = {goal: 0};
    public awardImageOpacity:number = 0.2;
    public presentAward:boolean = false;

    constructor(public nav:NavController,
                public params:NavParams,
                public viewCtrl:ViewController,
                public zone:NgZone,
                public translate:TranslateService) {
        super();
    }

    ngOnInit():void {
        this.awardInfo = this.params.data.awardInfo;
        this.presentAward = this.params.data.presentAward;
        this.translateParam.goal = this.awardInfo.goal;

        var progressText:string = "<div style='color:" + this.awardInfo.color + "; text-align:center; font-size:36px;'>" +
            this.awardInfo.current +
            "</div><div style='color:#333; text-align:center; font-size:22px;'>----<br>" +
            this.awardInfo.goal + "</div>";

        if (this.awardInfo.progress >= 1) {
            progressText = Constants.EMPTY_STRING;
            this.awardImageOpacity = 1;
        }

        var awardProgressBar = new ProgressBar.Circle(this.awardProgressElement.nativeElement, {
                color: this.awardInfo.color,
                trailColor: '#FFEA82',
                trailWidth: 1,
                duration: 1400,
                easing: 'bounce',
                strokeWidth: 6,
                text: {
                    value: progressText,
                    style: {
                        color: this.awardInfo.color,
                        position: 'absolute',
                        left: '50%',
                        top: '50%',
                        padding: 0,
                        margin: 0,
                        // You can specify styles which will be browser prefixed
                        transform: {
                            prefix: true,
                            value: 'translate(-50%, -50%)'
                        }
                    }
                }
            }
        );
        awardProgressBar.animate(this.awardInfo.progress);
    }

    private getBackgroundImageUrl():string {
        return "url(" + this.awardInfo.imageUri + ")";
    }

    private dismiss():void {
        this.viewCtrl.dismiss();
    }
}

export interface IAwardProgressModalPageParams {
    awardInfo:IAwardInfo,
    presentAward?:boolean
}