#!/usr/bin/env bash
# This .sh file will be sourced before starting your application.
# You can use it to put environment variables you want accessible
# to the server side of your app by using process.env.MY_VAR
#
# Example:
# export MONGO_URL="mongodb://localhost:27017/myapp-development"
# export ROOT_URL="http://localhost:3000"

export METEOR_SETTINGS=$(cat settings.json)
export PORT="3800"
export ROOT_URL="http://localhost:3800"
export MOBILE_DDP_URL=$ROOT_URL
export MAIL_URL="smtp://postmaster%40sandbox745f81b66c23497eb7b3dc48c04b2530.mailgun.org:c799b6dc5647f2ebc90eeb83f9b1ad68@smtp.mailgun.org:587"
export MONGO_URL="mongodb://localhost:27017/fiftyniner-dev"

