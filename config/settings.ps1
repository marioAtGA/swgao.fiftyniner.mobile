$webConfig = 'C:\Apps\Installation\GA\Portal\Mobile\PaymentApp\web.config'
$doc = (Get-Content $webConfig) -as [Xml]
$obj = $doc.configuration.appSettings.add | where {$_.Key -eq 'METEOR_SETTINGS'}
$settingsString = (Get-Content "settings.json") -join ''
echo "settings: $settingsString"
$obj.value = $settingsString
$doc.Save($webConfig)